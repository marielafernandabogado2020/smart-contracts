compile:
	./compile.sh

start-sandbox:
	docker-compose up -d
	@echo "Waiting 5s for container to launch ..."
	@sleep 5

kill-sandbox:
	docker-compose down

test-marketplace: compile kill-sandbox start-sandbox
	cd tests; \
	python3 test_marketplace.py

test-factory: compile kill-sandbox start-sandbox
	cd tests; \
	python3 test_factory.py

deploy-factory: compile
	python3 deploy_factory.py $(network)

deploy-marketplace: compile
	python3 deploy_marketplace.py $(network)