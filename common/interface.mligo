#if !COMMON_INTERFACE
#define COMMON_INTERFACE

type token_symbol = string

type fun_token =
[@layout:comb]
{
  token_symbol : token_symbol;
  fa_address : address;
  fa_type : string;
}

type token_id = nat

type offer_id =
[@layout:comb]
{
  token_id : token_id;
  buyer : address;
}

type counter_offer_id = 
[@layout:comb]
{
  token_id : token_id;
  buyer : address;
  seller : address;
}

type transfer_destination =
[@layout:comb]
{
  to_ : address;
  token_id : token_id;
  amount : nat;
}

type transfer =
[@layout:comb]
{
  from_ : address;
  txs : transfer_destination list;
}

type swap_id = nat

type royalties_amount = nat

type entrypoint_signature =
[@layout:comb]
{
    name : string;
    params : bytes;
    source_contract : address;
}

type call_param =
[@layout:comb]
{
    entrypoint_signature : entrypoint_signature;
    callback : unit -> operation list;
}

type update_royalties_param = nat

type update_fee_param =  nat 

type update_nft_address_param = address 

type update_royalties_address_param = address

type update_treasury_address_param = address

type add_token_param =
{
  fa_address : address;
  fa_type : string;
}

type set_oracle_tolerance_param = int

type update_multisig_param = address

type update_token_direction =
| Add_token of add_token_param
| Remove_token of unit

type update_allowed_tokens_param = 
[@layout:comb]
{
  token_symbol : token_symbol;
  direction : update_token_direction;
}

type accept_offer_param = 
[@layout:comb] 
{ 
  buyer : address;
  token_id : token_id;
} 

type collect_auction_param = swap_id

type collect_marketplace_param = 
[@layout:comb]
{
  swap_id : swap_id;
  token_symbol : token_symbol;
  amount_ft : nat; 
}

type token_contract_transfer = (address * (address * (token_id * nat)) list) list 

type fa12_contract_transfer =
[@layout:comb]
  { [@annot:from] address_from : address;
    [@annot:to] address_to : address;
    value : nat }

#endif