let get_set (set_id : Factory.set_id) (store : storage) : Factory.deployed_set =
    match (Tezos.call_view "get_set_view" set_id store.factory : Factory.deployed_set option) with
    | None -> (failwith("set_id does not match any factory set") : Factory.deployed_set)
    | Some set -> set

let prepare_multisig (type p) (entrypoint_name: string) (param: p) (func: unit -> operation list) (store : storage) : operation list =
    match (Tezos.get_entrypoint_opt "%callMultisig" store.multisig : Factory.call_param contract option ) with
    | None -> (failwith("no call entrypoint") : operation list)
    | Some contract ->
        let packed = Bytes.pack param in
        let param_hash = Crypto.sha256 packed in
        let entrypoint_signature =
          {
            name = entrypoint_name;
            params = param_hash;
            source_contract = (Tezos.get_self_address ());
          }
        in
        let call_param =
        {
          entrypoint_signature = entrypoint_signature;
          callback = func;
        }
        in
        let set_storage = Tezos.transaction call_param 0mutez contract in
        [set_storage]

let get_set_factory_entrypoint (contract_address : address) : address contract =
  match (Tezos.get_entrypoint_opt "%setFactory" contract_address : address contract option) with
  | None -> (failwith("contract has no setfactory entrypoint") : address contract)
  | Some contr -> contr