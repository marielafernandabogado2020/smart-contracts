#import "../../factory.mligo" "Factory"
#import "../../../mint/mint_interface.mligo" "Mint"
#include "mint_controller_interface.mligo"
#include "../common/common_functions.mligo"



let set_mint_params (param : set_mint_params) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setMintParams" sender_address : set_mint_params contract option) with
            | None -> (failwith("no setMintParams entrypoint") : operation list)
            | Some set_mint_params_entrypoint -> [Tezos.transaction param 0mutez set_mint_params_entrypoint] in
        (prepare_multisig "setMintParams" param func store), store
    else
        let {
            set_mint_params;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%setMintParams" set.minter : Mint.set_mint_params_param contract option) with
        | None -> (failwith("no setMintParams for this minter") : return)
        | Some contr -> [Tezos.transaction set_mint_params 0mutez contr], store

let set_mint_per_address (param : set_mint_per_address_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setMintPerAddress" sender_address : set_mint_per_address_param contract option) with
            | None -> (failwith("no setMintPerAddress entrypoint") : operation list)
            | Some set_mint_per_address_entrypoint -> [Tezos.transaction param 0mutez set_mint_per_address_entrypoint] in
        (prepare_multisig "setMintPerAddress" param func store), store
    else
        let set = get_set param store in
        match (Tezos.get_entrypoint_opt "%setMintPerAddress" set.minter : unit contract option) with
        | None -> (failwith("no setMintPerAddress for this minter") : return)
        | Some contr -> [Tezos.transaction () 0mutez contr], store

let add_to_whitelist (param : add_to_whitelist_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%addToWhitelist" sender_address : add_to_whitelist_param contract option) with
            | None -> (failwith("no addToWhitelist entrypoint") : operation list)
            | Some add_to_whitelist_entrypoint -> [Tezos.transaction param 0mutez add_to_whitelist_entrypoint] in
        (prepare_multisig "addToWhitelist" param func store), store
    else
        let {
            add_to_whitelist_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%addToWhitelist" set.minter : Mint.add_to_whitelist_param contract option) with
        | None -> (failwith("no addToWhitelist for this minter") : return)
        | Some contr -> [Tezos.transaction add_to_whitelist_param 0mutez contr], store

let remove_from_whitelist (param : remove_from_whitelist_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%removeFromWhitelist" sender_address : remove_from_whitelist_param contract option) with
            | None -> (failwith("no removeFromWhitelist entrypoint") : operation list)
            | Some remove_from_whitelist_entrypoint -> [Tezos.transaction param 0mutez remove_from_whitelist_entrypoint] in
        (prepare_multisig "removeFromWhitelist" param func store), store
    else
        let {
            remove_from_whitelist_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%removeFromWhitelist" set.minter : Mint.remove_from_whitelist_param contract option) with
        | None -> (failwith("no removeFromWhitelist for this minter") : return)
        | Some contr -> [Tezos.transaction remove_from_whitelist_param 0mutez contr], store

let set_default_metadata (param : set_default_metadata_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setDefaultMetadata" sender_address : set_default_metadata_param contract option) with
            | None -> (failwith("no setDefaultMetadata entrypoint") : operation list)
            | Some set_default_metadata_entrypoint -> [Tezos.transaction param 0mutez set_default_metadata_entrypoint] in
        (prepare_multisig "setDefaultMetadata" param func store), store
    else
        let {
            set_default_metadata_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%setDefaultMetadata" set.minter : Mint.set_default_metadata_param contract option) with
        | None -> (failwith("no setDefaultMetadata for this minter") : return)
        | Some contr -> [Tezos.transaction set_default_metadata_param 0mutez contr], store

let burn_from_crowdsale (param : burn_from_crowdsale_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%burnFromCrowdsale" sender_address : burn_from_crowdsale_param contract option) with
            | None -> (failwith("no burnFromCrowdsale entrypoint") : operation list)
            | Some burn_from_crowdsale_entrypoint -> [Tezos.transaction param 0mutez burn_from_crowdsale_entrypoint] in
        (prepare_multisig "burnFromCrowdsale" param func store), store
    else
        let {
            burn_from_crowdsale_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%burnFromCrowdsale" set.minter : Mint.burn_from_crowdsale_param contract option) with
        | None -> (failwith("no burnFromCrowdsale for this minter") : return)
        | Some contr -> [Tezos.transaction burn_from_crowdsale_param 0mutez contr], store

let set_name_list (param : set_name_list_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setNameList" sender_address : set_name_list_param contract option) with
            | None -> (failwith("no setNameList entrypoint") : operation list)
            | Some set_name_list_entrypoint -> [Tezos.transaction param 0mutez set_name_list_entrypoint] in
        (prepare_multisig "setNameList" param func store), store
    else
        let {
            set_name_list_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%setNameList" set.minter : Mint.set_name_list_param contract option) with
        | None -> (failwith("no setNameList for this minter") : return)
        | Some contr -> [Tezos.transaction set_name_list_param 0mutez contr], store

let get_set_doga_address_entrypoint (contract_address : address) : address contract =
    match (Tezos.get_entrypoint_opt "%setDogaAddress" contract_address : address contract option) with
    | None -> (failwith("no setDogaAddress entrypoint for the chosen contract") : address contract)
    | Some contr -> contr

let set_doga_address_per_set (set_id : set_id) (doga_address : address) (operations : operation list) (store : storage) : operation list =
    let set = get_set set_id store in
    Tezos.transaction doga_address 0mutez (get_set_doga_address_entrypoint set.minter) :: operations


let set_doga_address (param : set_doga_address_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setDogaAddress" sender_address : set_doga_address_param contract option) with
            | None -> (failwith("no setDogaAddress entrypoint") : operation list)
            | Some set_doga_address_entrypoint -> [Tezos.transaction param 0mutez set_doga_address_entrypoint] in
        (prepare_multisig "setDogaAddress" param func store), store
    else
        let {
            update_method;
            doga_address;
        } = param in
        let ops = ([] : operation list) in
        let ops =
            match update_method with
            | Single set_id ->
                set_doga_address_per_set set_id doga_address ops store
            | Multiple set_ids ->
                List.fold (
                    fun (operations, set_id : operation list * set_id) ->
                        set_doga_address_per_set set_id doga_address operations store
                ) set_ids ops in
        ops, store

let get_set_reserve_address_entrypoint (contract_address : address) : address contract =
    match (Tezos.get_entrypoint_opt "%setReserveAddress" contract_address : address contract option) with
    | None -> (failwith("no setReserveAddress entrypoint for the chosen contract") : address contract)
    | Some contr -> contr

let set_reserve_address_per_set (set_id : set_id) (reserve_address : address) (operations : operation list) (store : storage) : operation list =
    let set = get_set set_id store in
    Tezos.transaction reserve_address 0mutez (get_set_reserve_address_entrypoint set.minter) :: operations


let set_reserve_address (param : set_reserve_address_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setReserveAddress" sender_address : set_reserve_address_param contract option) with
            | None -> (failwith("no setReserveAddress entrypoint") : operation list)
            | Some set_reserve_address_entrypoint -> [Tezos.transaction param 0mutez set_reserve_address_entrypoint] in
        (prepare_multisig "setReserveAddress" param func store), store
    else
        let {
            update_method;
            reserve_address;
        } = param in
        let ops = ([] : operation list) in
        let ops =
            match update_method with
            | Single set_id ->
                set_reserve_address_per_set set_id reserve_address ops store
            | Multiple set_ids ->
                List.fold (
                    fun (operations, set_id : operation list * set_id) ->
                        set_reserve_address_per_set set_id reserve_address operations store
                ) set_ids ops in
        ops, store

let update_multisig (param : address) (store : storage) : return =
    if (Tezos.get_sender () <> store.factory) then
        (failwith("only factory can call this entrypoint") : return)
    else
        ([] : operation list), {store with multisig = param}

let update_factory (param : address) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%updateFactory" sender_address : address contract option) with
            | None -> (failwith("no updateFactory entrypoint") : operation list)
            | Some update_factory_entrypoint -> [Tezos.transaction param 0mutez update_factory_entrypoint] in
        (prepare_multisig "updateFactory" param func store), store
    else
        ([] : operation list), {store with factory = param}

let main (action, store : parameter * storage) : return =
match action with
| SetMintParams p -> set_mint_params p store
| SetMintPerAddress p -> set_mint_per_address p store
| AddToWhitelist p -> add_to_whitelist p store
| RemoveFromWhitelist p -> remove_from_whitelist p store
| SetDefaultMetadata p -> set_default_metadata p store
| BurnFromCrowdsale p -> burn_from_crowdsale p store
| SetNameList p -> set_name_list p store
| SetDogaAddress p -> set_doga_address p store
| SetReserveAddress p -> set_reserve_address p store
| UpdateMultisig p -> update_multisig p store
| UpdateFactory p -> update_factory p store
