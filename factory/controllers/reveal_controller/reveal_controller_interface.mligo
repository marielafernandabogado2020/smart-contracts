type set_id = Factory.set_id

type storage =
[@layout:comb]
{
    factory : address;
    multisig : address;
}

type set_oracle_address_param =
[@layout:comb]
{
    set_oracle_address_param : Reveal.set_oracle_address_param;
    set_id : set_id;
}

type set_free_metadata_param =
[@layout:comb]
{
    set_free_metadata_param : Reveal.set_free_metadata_param;
    set_id : set_id;
}

type set_token_id_to_metadata_id_param =
[@layout:comb]
{
    set_token_id_to_metadata_id_param : Reveal.set_token_id_to_metadata_id_param;
    set_id : set_id;
}

type set_random_offset_param =
[@layout:comb]
{
    set_random_offset_param : Reveal.set_random_offset_param;
    set_id : set_id;
}

type add_reveal_admin_param =
[@layout:comb]
{
    add_reveal_admin_param : Reveal.add_reveal_admin_param;
    set_id : set_id;
}

type remove_reveal_admin_param =
[@layout:comb]
{
    remove_reveal_admin_param : Reveal.remove_reveal_admin_param;
    set_id : set_id;
}

type set_ipfs_hashes_param =
[@layout:comb]
{
    set_ipfs_hashes_param : Reveal.set_ipfs_hashes_param;
    set_id : set_id;
}

type parameter =
| SetOracleAddress of set_oracle_address_param
| SetFreeMetadata of set_free_metadata_param
| SetTokenIdToMetadataId of set_token_id_to_metadata_id_param
| SetRandomOffset of set_random_offset_param
| AddRevealAdmin of add_reveal_admin_param
| RemoveRevealAdmin of remove_reveal_admin_param
| SetIpfsHashes of set_ipfs_hashes_param
| UpdateMultisig of address
| UpdateFactory of address

type return = operation list * storage