
#if !FACTORY
#define FACTORY

#include "factory_interface.mligo"
#include "factory_functions.mligo"

(* updaters for both contracts *)

let get_set (set_id : set_id) (store : storage) : deployed_set =
    match Big_map.find_opt set_id store.contract_sets with
    | None -> (failwith("set name does not exist") : deployed_set)
    | Some set -> set

let get_set_pause_entrypoint (contract_address : address) : bool contract =
    match (Tezos.get_entrypoint_opt "%setPause" contract_address : bool contract option) with
    | None -> (failwith("no setPause entrypoint for the chosen contract") : bool contract)
    | Some contr -> contr

let set_pause_per_set (set_id : set_id) (choose_contract : choose_contract) (pause : bool) (operations : operation list) (store : storage) : operation list =
    let set = get_set set_id store in
    match choose_contract with
    | Minter -> Tezos.transaction pause 0mutez (get_set_pause_entrypoint set.minter) :: operations
    | Revealer -> Tezos.transaction pause 0mutez (get_set_pause_entrypoint set.revealer) :: operations
    | Both ->
        let operations = Tezos.transaction pause 0mutez (get_set_pause_entrypoint set.minter) :: operations in
        let operations = Tezos.transaction pause 0mutez (get_set_pause_entrypoint set.revealer) :: operations in
        operations

let set_pause (param : set_pause_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setPause" sender_address : set_pause_param contract option) with
            | None -> (failwith("no setPause entrypoint") : operation list)
            | Some set_pause_entrypoint -> [Tezos.transaction param 0mutez set_pause_entrypoint] in
        (prepare_multisig "setPause" param func store), store
    else
        let {
                update_method;
                choose_contract;
                pause;
        } = param in
        let ops = ([] : operation list) in
        let ops =
            match update_method with
            | Single set_id ->
                set_pause_per_set set_id choose_contract pause ops store
            | Multiple set_ids ->
                List.fold (
                    fun (operations, set_id : operation list * set_id) ->
                        set_pause_per_set set_id choose_contract pause operations store
                ) set_ids ops in
        ops, store

let set_nft_address (param : set_nft_address_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setNftAddress" sender_address : set_nft_address_param contract option) with
            | None -> (failwith("no setNftAddress entrypoint") : operation list)
            | Some set_nft_address_entrypoint -> [Tezos.transaction param 0mutez set_nft_address_entrypoint] in
        (prepare_multisig "setNftAddress" param func store), store
    else
        let {
            set_id;
            nft_address;
        } = param in
        let set = get_set set_id store in
        let ops = ([] : operation list) in
        let ops = Tezos.transaction nft_address 0mutez (get_set_nft_address_entrypoint set.minter) :: ops in
        let ops = Tezos.transaction nft_address 0mutez (get_set_nft_address_entrypoint set.revealer) :: ops in
        let ops = Tezos.transaction set.minter 0mutez (get_add_contract_entrypoint nft_address) :: ops in
        let ops = Tezos.transaction set.revealer 0mutez (get_add_contract_entrypoint nft_address) :: ops in
        let set = {set with nft = nft_address} in
        let contract_sets = Big_map.update set_id (Some set) store.contract_sets in
        ops, {store with contract_sets = contract_sets}

let set_minter_address (param : set_minter_address_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setMinterAddress" sender_address : set_minter_address_param contract option) with
            | None -> (failwith("no setMinterAddress entrypoint") : operation list)
            | Some set_minter_address_entrypoint -> [Tezos.transaction param 0mutez set_minter_address_entrypoint] in
        (prepare_multisig "setMinterAddress" param func store), store
    else
        let {
            set_id;
            minter_address;
        } = param in
        let set = get_set set_id store in
        let ops = ([] : operation list) in
        let ops = Tezos.transaction set.nft 0mutez (get_set_nft_address_entrypoint minter_address) :: ops in
        let ops = Tezos.transaction minter_address 0mutez (get_add_contract_entrypoint set.nft) :: ops in
        let ops = Tezos.transaction set.minter 0mutez (get_remove_contract_entrypoint set.nft) :: ops in
        let set = {set with minter = minter_address} in
        let contract_sets = Big_map.update set_id (Some set) store.contract_sets in
        ops, {store with contract_sets = contract_sets}

let set_revealer_address (param : set_revealer_address_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setRevealerAddress" sender_address : set_revealer_address_param contract option) with
            | None -> (failwith("no setRevealerAddress entrypoint") : operation list)
            | Some set_revealer_address_entrypoint -> [Tezos.transaction param 0mutez set_revealer_address_entrypoint] in
        (prepare_multisig "setRevealerAddress" param func store), store
    else
        let {
            set_id;
            revealer_address;
        } = param in
        let set = get_set set_id store in
        let ops = ([] : operation list) in
        let ops = Tezos.transaction set.nft 0mutez (get_set_nft_address_entrypoint revealer_address) :: ops in
        let ops = Tezos.transaction revealer_address 0mutez (get_add_contract_entrypoint set.nft) :: ops in
        let ops = Tezos.transaction set.revealer 0mutez (get_remove_contract_entrypoint set.nft) :: ops in
        let set = {set with revealer = revealer_address} in
        let contract_sets = Big_map.update set_id (Some set) store.contract_sets in
        ops, {store with contract_sets = contract_sets}

let update_multisig (param : address) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%updateMultisig" sender_address : address contract option) with
            | None -> (failwith("no updateMultisig entrypoint") : operation list)
            | Some update_multisig_entrypoint -> [Tezos.transaction param 0mutez update_multisig_entrypoint] in
            (prepare_multisig "updateMultisig" param func store), store
    else
        let ops = ([] : operation list) in
        let ops = Tezos.transaction param 0mutez (get_update_multisig_entrypoint store.mint_controller) :: ops in
        let ops = Tezos.transaction param 0mutez (get_update_multisig_entrypoint store.reveal_controller) :: ops in
        let ops = Tezos.transaction param 0mutez (get_update_multisig_entrypoint store.nft_controller) :: ops in
        ops, { store with multisig = param }

let deploy_set (param : deploy_set_param) (store : storage) : return =
        match param with
        | Deploy set_id ->
            if (Tezos.get_sender ()) <> store.multisig then
                let sender_address = (Tezos.get_self_address ()) in
                let func () =
                  match (Tezos.get_entrypoint_opt "%deploySet" sender_address : deploy_set_param contract option) with
                  | None -> (failwith("no deploySet entrypoint") : operation list)
                  | Some deploy_set_entrypoint -> [Tezos.transaction param 0mutez deploy_set_entrypoint] in
                (prepare_multisig "deploySet" param func store), store
            else
                let launch_minter_params =
                    {
                        doga_address = store.doga_address;
                        reserve = store.default_reserve;
                        set_id = set_id;
                    } in
                let launcher =
                    match (Tezos.get_contract_opt store.mint_launcher : MintLauncher.launcher_parameter contract option) with
                    | None -> (failwith("mint launcher was not deployed correctly") : MintLauncher.launcher_parameter contract)
                    | Some contr -> contr in
                [Tezos.transaction launch_minter_params 0mutez launcher], store
        | AfterMinter p ->
            if Tezos.get_sender () <> store.mint_launcher then
                (failwith("only mint launcher can call afterMinter") : return)
            else
                let {minter_address; set_id} = p in
                let store = {store with last_deployed_contracts.minter = minter_address} in

                let launch_revealer_params =
                    {
                        oracle = store.default_oracle;
                        set_id = set_id;
                    } in
                let launcher =
                    match (Tezos.get_contract_opt store.reveal_launcher : RevealLauncher.launcher_param contract option) with
                    | None -> (failwith("reveal launcher was not deployed correctly") : RevealLauncher.launcher_param contract)
                    | Some contr -> contr in
                [Tezos.transaction launch_revealer_params 0mutez launcher], store
        | AfterRevealer p ->
            if Tezos.get_sender () <> store.reveal_launcher then
                (failwith("only reveal launcher can call afterRevealer") : return)
            else
                let {revealer_address; set_id} = p in
                let store = {store with last_deployed_contracts.revealer = revealer_address} in

                let launch_nft_params =
                    {
                        minter_address = store.last_deployed_contracts.minter;
                        revealer_address = revealer_address;
                        set_id = set_id;
                    } in
                let launcher =
                    match (Tezos.get_contract_opt store.nft_launcher : NftLauncher.launcher_param contract option) with
                    | None -> (failwith("nft launcher was not deployed correctly") : NftLauncher.launcher_param contract)
                    | Some contr -> contr in
                [Tezos.transaction launch_nft_params 0mutez launcher], store
        | AfterNft p ->
            if Tezos.get_sender () <> store.nft_launcher then
                (failwith("only nft launcher can call afterNft") : return)
            else
            let {nft_address; set_id} = p in
            let store = {store with last_deployed_contracts.nft = nft_address} in
            let op_add_nft_to_multisig = add_multisig_authorized_contract nft_address store.multisig in
            let set = store.last_deployed_contracts in
            let op_set_nft_address_minter = Tezos.transaction nft_address 0mutez (get_set_nft_address_entrypoint set.minter) in
            let op_set_nft_address_revealer = Tezos.transaction nft_address 0mutez (get_set_nft_address_entrypoint set.revealer) in
            let contract_sets =
                match Big_map.find_opt set_id store.contract_sets with
                | Some _index -> (failwith("contract set name exists") : (string, deployed_set) big_map)
                | None -> Big_map.update set_id (Some set) store.contract_sets in
            let store =
                { store with contract_sets = contract_sets} in
            let ops =
                [
                    op_add_nft_to_multisig;
                    op_set_nft_address_minter;
                    op_set_nft_address_revealer;
                ] in
            ops, store

let update_launchers (param : update_launchers_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
          match (Tezos.get_entrypoint_opt "%updateLaunchers" sender_address : update_launchers_param contract option) with
          | None -> (failwith("no updateLaunchers entrypoint") : operation list)
          | Some update_launchers_entrypoint -> [Tezos.transaction param 0mutez update_launchers_entrypoint] in
        (prepare_multisig "updateLaunchers" param func store), store
    else
        ([] : operation list), {store with mint_launcher = param.mint_launcher; reveal_launcher = param.reveal_launcher; nft_launcher = param.nft_launcher}

let update_controllers (param : update_controllers_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
          match (Tezos.get_entrypoint_opt "%updateControllers" sender_address : update_controllers_param contract option) with
          | None -> (failwith("no updateControllers entrypoint") : operation list)
          | Some update_controllers_entrypoint -> [Tezos.transaction param 0mutez update_controllers_entrypoint] in
        (prepare_multisig "updateControllers" param func store), store
    else
        ([] : operation list), {store with mint_controller = param.mint_controller; reveal_controller = param.reveal_controller; nft_controller = param.nft_controller}

let set_doga_address (param : set_doga_address_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
          match (Tezos.get_entrypoint_opt "%setDogaAddress" sender_address : set_doga_address_param contract option) with
          | None -> (failwith("no setDogaAddress entrypoint") : operation list)
          | Some set_doga_address_entrypoint -> [Tezos.transaction param 0mutez set_doga_address_entrypoint] in
        (prepare_multisig "setDogaAddress" param func store), store
    else
        ([] : operation list), {store with doga_address = param}

let set_default_reserve (param : set_default_reserve_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
          match (Tezos.get_entrypoint_opt "%setDefaultReserve" sender_address : set_default_reserve_param contract option) with
          | None -> (failwith("no setDefaultReserve entrypoint") : operation list)
          | Some set_default_reserve_entrypoint -> [Tezos.transaction param 0mutez set_default_reserve_entrypoint] in
        (prepare_multisig "setDefaultReserve" param func store), store
    else
        ([] : operation list), {store with default_reserve = param}

let set_oracle_address (param : set_oracle_address_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
          match (Tezos.get_entrypoint_opt "%setOracleAddress" sender_address : set_oracle_address_param contract option) with
          | None -> (failwith("no setOracleAddress entrypoint") : operation list)
          | Some set_oracle_address_entrypoint -> [Tezos.transaction param 0mutez set_oracle_address_entrypoint] in
        (prepare_multisig "setOracleAddress" param func store), store
    else
        ([] : operation list), {store with default_oracle = param}

let main (action, store : parameter * storage) : return =
match action with
| DeploySet p -> deploy_set p store
| UpdateMultisig p -> update_multisig p store
| SetPause p -> set_pause p store
| SetNftAddress p -> set_nft_address p store
| SetDogaAddress p -> set_doga_address p store
| SetOracleAddress p -> set_oracle_address p store
| SetDefaultReserve p -> set_default_reserve p store
| SetRevealerAddress p -> set_revealer_address p store
| SetMinterAddress p -> set_minter_address p store
| UpdateLaunchers p -> update_launchers p store
| UpdateControllers p -> update_controllers p store


[@view] let get_set_view (set_id, store : set_id * storage) : deployed_set =
    match Big_map.find_opt set_id store.contract_sets with
    | None -> (failwith("get_set_view - no set") : deployed_set)
    | Some set -> set

#endif
