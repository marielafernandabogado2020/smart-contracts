let launch_minter (initial_storage : Mint.storage) : operation * address =
([%Michelson
    ({| {  UNPPAIIR ;
           CREATE_CONTRACT
#include "../michelson/mint.tz"
  ;
           PAIR } |} : 
       (key_hash option * tez * minter_storage) -> (operation * address))])
    ((None : key_hash option), 0mutez, initial_storage)

let launch_revealer (initial_storage : revealer_storage) : operation * address =
([%Michelson
    ({| {  UNPPAIIR ;
           CREATE_CONTRACT
#include "../michelson/reveal.tz"
  ;
           PAIR } |} : 
       (key_hash option * tez * revealer_storage) -> (operation * address))])
    ((None : key_hash option), 0mutez, initial_storage)

let launch_nft (initial_storage : nft_storage) : operation * address =
([%Michelson
    ({| {  UNPPAIIR ;
           CREATE_CONTRACT
#include "../michelson/nft.tz"
  ;
           PAIR } |} : 
       (key_hash option * tez * nft_storage) -> (operation * address))])
    ((None : key_hash option), 0mutez, initial_storage)


[@inline]
let prepare_multisig (type p) (entrypoint_name: string) (param: p) (func: unit -> operation list) (store : storage) : operation list =
    match (Tezos.get_entrypoint_opt "%callMultisig" store.multisig : call_param contract option ) with
    | None -> (failwith("no call entrypoint") : operation list)
    | Some contract ->
        let packed = Bytes.pack param in
        let param_hash = Crypto.sha256 packed in
        let entrypoint_signature =
          {
            name = entrypoint_name;
            params = param_hash;
            source_contract = (Tezos.get_self_address ());
          }
        in
        let call_param =
        {
          entrypoint_signature = entrypoint_signature;
          callback = func;
        }
        in
        let set_storage = Tezos.transaction call_param 0mutez contract in
        [set_storage]

let add_nft_contract (param : add_nft_contract_param) : operation list =
  let () = assert_with_error (Tezos.get_sender () = Tezos.get_self_address ()) "only self can call this entrypoint" in 
    let add_contract_entrypoint =
        match (Tezos.get_entrypoint_opt "%addContract" param.nft_address : address contract option) with
        | None -> (failwith("no add contract entrypoint") : address contract)
        | Some contr -> contr in
    let ops = ([] : operation list) in
    let ops =
      List.fold(fun (operations, addr : operation list * address) ->
        Tezos.transaction addr 0mutez add_contract_entrypoint :: operations) param.contract_addresses ops in
    ops

let get_add_contract_entrypoint (nft_address : address) : Nft.add_contract_param contract =
  match (Tezos.get_entrypoint_opt "%addContract" nft_address : Nft.add_contract_param contract option) with
  | None -> (failwith("nft contract has no addContract entrypoint") : Nft.add_contract_param contract)
  | Some contr -> contr

let get_remove_contract_entrypoint (nft_address : address) : Nft.add_contract_param contract =
  match (Tezos.get_entrypoint_opt "%removeContract" nft_address : Nft.remove_contract_param contract option) with
  | None -> (failwith("nft contract has no removeContract entrypoint") : Nft.remove_contract_param contract)
  | Some contr -> contr

let get_set_nft_address_entrypoint (contract_address : address) : address contract =
    match (Tezos.get_entrypoint_opt "%setNftAddress" contract_address : address contract option) with
    | None -> (failwith("no setNftAddress entrypoint for the chosen contract") : address contract)
    | Some contr -> contr 

let get_update_multisig_entrypoint (contract_address : address) : address contract =
  match (Tezos.get_entrypoint_opt "%updateMultisig" contract_address : address contract option) with
  | None -> (failwith("contract has no updateMultisig entrypoint") : address contract)
  | Some contr -> contr

let get_set_factory_entrypoint (contract_address : address) : address contract =
  match (Tezos.get_entrypoint_opt "%setFactory" contract_address : address contract option) with
  | None -> (failwith("contract has no setfactory entrypoint") : address contract)
  | Some contr -> contr

let get_set_controller_entrypoint (contract_address : address) : address contract =
  match (Tezos.get_entrypoint_opt "%setController" contract_address : address contract option) with
  | None -> (failwith("contract has no setController entrypoint") : address contract)
  | Some contr -> contr

let get_add_nft_contract (revealer : address) (minter : address) (nft_address : address) : operation =
  let entrypoint =
    match (Tezos.get_entrypoint_opt "%addNftContract" (Tezos.get_self_address ()) : add_nft_contract_param contract option) with
    | None -> (failwith("no self add nft contract entrypoint") : add_nft_contract_param contract)
    | Some contr -> contr in
  let add_params =
    {
      nft_address = nft_address;
      contract_addresses = [revealer; minter];
    } in
  Tezos.transaction add_params 0mutez entrypoint
    

let add_multisig_authorized_contract (contract_address : address) (multisig : address) : operation =
  let add_authorized_contract_entrypoint =
    match (Tezos.get_entrypoint_opt "%addAuthorizedContract" multisig : address contract option) with
    | None -> (failwith("no add authorized contract entrypoint") : address contract)
    | Some contr -> contr in
  Tezos.transaction contract_address 0mutez add_authorized_contract_entrypoint
