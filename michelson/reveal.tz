{ parameter
    (or (or (or (or (address %addRevealAdmin) (nat %assignMetadata))
                (or (address %removeRevealAdmin)
                    (pair %reveal
                       (pair %metadata
                          (pair (pair (bytes %artifactUri)
                                      (pair %attributes
                                         (pair (pair (pair (pair (string %a) (string %b)) (string %c) (string %d))
                                                     (pair (string %e) (string %f))
                                                     (string %g)
                                                     (string %h))
                                               (pair (pair (string %i) (string %j)) (string %k) (string %l))
                                               (pair (string %m) (string %n))
                                               (string %o)
                                               (string %p))
                                         (pair (string %q) (string %r))
                                         (string %s)
                                         (string %t)))
                                (bytes %displayUri)
                                (bytes %formats))
                          (nat %metadata_id)
                          (bytes %thumbnailUri))
                       (nat %token_id))))
            (or (or (pair %setFreeMetadata
                       (pair (map %free_metadata nat nat) (nat %free_metadata_length))
                       (nat %version))
                    (string %setIpfsHashes))
                (or (address %setNftAddress) (address %setOracleAddress))))
        (or (or (bool %setPause) (pair %setRandomOffset (nat %from_) (nat %to_)))
            (map %setTokenIdToMetadataId nat nat))) ;
  storage
    (pair (bool %paused)
          (address %controller)
          (address %factory)
          (address %nft_address)
          (timestamp %reveal_time)
          (big_map %free_metadata_v1 nat nat)
          (nat %free_metadata_v1_length)
          (big_map %free_metadata_v2 nat nat)
          (nat %free_metadata_v2_length)
          (big_map %token_id_to_metadata_id nat nat)
          (nat %random_offset)
          (set %reveal_admins address)
          (address %oracle)
          (string %ipfs_hashes)) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { DUP 2 ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { DROP 2 ; PUSH string "only controller can call this entrypoint" ; FAILWITH }
                            { DUP 2 ;
                              DIG 2 ;
                              GET 23 ;
                              DIG 2 ;
                              PUSH bool True ;
                              SWAP ;
                              UPDATE ;
                              UPDATE 23 ;
                              NIL operation ;
                              PAIR } }
                       { PUSH bool True ;
                         DUP 3 ;
                         CAR ;
                         COMPARE ;
                         EQ ;
                         IF { DROP 2 ; PUSH string "Contract in pause" ; FAILWITH }
                            { DUP ;
                              SENDER ;
                              PAIR ;
                              DUP 3 ;
                              GET 19 ;
                              DUP 3 ;
                              GET ;
                              IF_NONE
                                {}
                                { DROP ; PUSH string "Token already has an assigned metadata" ; FAILWITH } ;
                              DUP 3 ;
                              GET 7 ;
                              SWAP ;
                              VIEW "balance_of_view" nat ;
                              IF_NONE
                                { DROP 2 ; PUSH string "View returned an error" ; FAILWITH }
                                { PUSH nat 1 ;
                                  SWAP ;
                                  COMPARE ;
                                  NEQ ;
                                  IF { DROP 2 ; PUSH string "You do not own this token" ; FAILWITH }
                                     { PUSH nat 8000 ;
                                       DUP 2 ;
                                       COMPARE ;
                                       LE ;
                                       IF { PUSH nat 1 ;
                                            DUP 3 ;
                                            GET 13 ;
                                            COMPARE ;
                                            LT ;
                                            IF { DROP 2 ; PUSH string "No free metadata available" ; FAILWITH }
                                               { LEVEL ;
                                                 DUP 3 ;
                                                 GET 21 ;
                                                 ADD ;
                                                 PUSH nat 1 ;
                                                 DUP 4 ;
                                                 GET 13 ;
                                                 DIG 2 ;
                                                 EDIV ;
                                                 IF_NONE { PUSH string "MOD by 0" ; FAILWITH } {} ;
                                                 CDR ;
                                                 ADD ;
                                                 DUP 3 ;
                                                 GET 11 ;
                                                 DUP 2 ;
                                                 GET ;
                                                 IF_NONE { PUSH string "Index does not exist" ; FAILWITH } {} ;
                                                 DUP 4 ;
                                                 GET 11 ;
                                                 DUP 5 ;
                                                 GET 13 ;
                                                 GET ;
                                                 IF_NONE { PUSH string "Did not find tail metadata id" ; FAILWITH } {} ;
                                                 PUSH int 1 ;
                                                 DUP 6 ;
                                                 GET 13 ;
                                                 SUB ;
                                                 ISNAT ;
                                                 DUP 6 ;
                                                 DUP 4 ;
                                                 UPDATE 21 ;
                                                 DUP 7 ;
                                                 GET 19 ;
                                                 DIG 4 ;
                                                 SOME ;
                                                 DIG 6 ;
                                                 UPDATE ;
                                                 UPDATE 19 ;
                                                 DUP 5 ;
                                                 GET 11 ;
                                                 DIG 3 ;
                                                 SOME ;
                                                 DIG 4 ;
                                                 UPDATE ;
                                                 NONE nat ;
                                                 DIG 4 ;
                                                 GET 13 ;
                                                 UPDATE ;
                                                 UPDATE 11 ;
                                                 SWAP ;
                                                 IF_NONE { PUSH nat 0 } {} ;
                                                 UPDATE 13 ;
                                                 NIL operation ;
                                                 PAIR } }
                                          { PUSH nat 12000 ;
                                            DUP 2 ;
                                            COMPARE ;
                                            LE ;
                                            IF { NOW ;
                                                 DUP 3 ;
                                                 GET 9 ;
                                                 COMPARE ;
                                                 GT ;
                                                 IF { DROP 2 ; PUSH string "Reveal period is not open" ; FAILWITH }
                                                    { PUSH nat 1 ;
                                                      DUP 3 ;
                                                      GET 17 ;
                                                      COMPARE ;
                                                      LT ;
                                                      IF { DROP 2 ; PUSH string "No free metadata available" ; FAILWITH }
                                                         { LEVEL ;
                                                           DUP 3 ;
                                                           GET 21 ;
                                                           ADD ;
                                                           PUSH nat 1 ;
                                                           DUP 4 ;
                                                           GET 17 ;
                                                           DIG 2 ;
                                                           EDIV ;
                                                           IF_NONE { PUSH string "MOD by 0" ; FAILWITH } {} ;
                                                           CDR ;
                                                           ADD ;
                                                           DUP 3 ;
                                                           GET 15 ;
                                                           DUP 2 ;
                                                           GET ;
                                                           IF_NONE { PUSH string "Index does not exist" ; FAILWITH } {} ;
                                                           DUP 4 ;
                                                           GET 15 ;
                                                           DUP 5 ;
                                                           GET 17 ;
                                                           GET ;
                                                           IF_NONE { PUSH string "Did not find tail metadata id" ; FAILWITH } {} ;
                                                           PUSH int 1 ;
                                                           DUP 6 ;
                                                           GET 17 ;
                                                           SUB ;
                                                           ISNAT ;
                                                           DUP 6 ;
                                                           DUP 4 ;
                                                           UPDATE 21 ;
                                                           DUP 7 ;
                                                           GET 19 ;
                                                           DIG 4 ;
                                                           SOME ;
                                                           DIG 6 ;
                                                           UPDATE ;
                                                           UPDATE 19 ;
                                                           DUP 5 ;
                                                           GET 15 ;
                                                           DIG 3 ;
                                                           SOME ;
                                                           DIG 4 ;
                                                           UPDATE ;
                                                           NONE nat ;
                                                           DIG 4 ;
                                                           GET 17 ;
                                                           UPDATE ;
                                                           UPDATE 15 ;
                                                           SWAP ;
                                                           IF_NONE { PUSH nat 0 } {} ;
                                                           UPDATE 17 ;
                                                           NIL operation ;
                                                           PAIR } } }
                                               { DROP 2 ; PUSH string "token_id out of range" ; FAILWITH } } } } } } }
                   { IF_LEFT
                       { DUP 2 ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { DROP 2 ; PUSH string "only controller can call this entrypoint" ; FAILWITH }
                            { DUP 2 ;
                              DIG 2 ;
                              GET 23 ;
                              DIG 2 ;
                              PUSH bool False ;
                              SWAP ;
                              UPDATE ;
                              UPDATE 23 ;
                              NIL operation ;
                              PAIR } }
                       { DUP 2 ;
                         GET 23 ;
                         SENDER ;
                         MEM ;
                         NOT ;
                         IF { DROP 2 ; PUSH string "Unauthorized admin address" ; FAILWITH }
                            { DUP 2 ;
                              GET 19 ;
                              DUP 2 ;
                              CDR ;
                              GET ;
                              IF_NONE
                                { PUSH string "You have not assigned a metadata yet" ; FAILWITH }
                                { DUP 2 ;
                                  CAR ;
                                  CDR ;
                                  CAR ;
                                  SWAP ;
                                  COMPARE ;
                                  NEQ ;
                                  IF { PUSH string "Token id not matching metadata id" ; FAILWITH } {} } ;
                              DUP 2 ;
                              GET 7 ;
                              CONTRACT %updateMetadataWithFunction
                                (pair (lambda %metadata_updater (map string bytes) (map string bytes)) (nat %token_id)) ;
                              IF_NONE
                                { DROP ;
                                  PUSH string "NFT contract must have an update metadata with function entrypoint" ;
                                  FAILWITH }
                                { PUSH mutez 0 ;
                                  DUP 3 ;
                                  CDR ;
                                  LAMBDA
                                    (pair (pair (pair (pair (pair bytes
                                                                  (pair (pair (pair (pair (pair string string) string string) (pair string string) string string)
                                                                              (pair (pair string string) string string)
                                                                              (pair string string)
                                                                              string
                                                                              string)
                                                                        (pair string string)
                                                                        string
                                                                        string))
                                                            bytes
                                                            bytes)
                                                      nat
                                                      bytes)
                                                nat)
                                          (map string bytes))
                                    (map string bytes)
                                    { UNPAIR ;
                                      SWAP ;
                                      DUP 2 ;
                                      CAR ;
                                      CAR ;
                                      CDR ;
                                      CDR ;
                                      SOME ;
                                      PUSH string "formats" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CAR ;
                                      CAR ;
                                      CAR ;
                                      SOME ;
                                      PUSH string "artifactUri" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CAR ;
                                      CDR ;
                                      CAR ;
                                      SOME ;
                                      PUSH string "displayUri" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CDR ;
                                      CDR ;
                                      SOME ;
                                      PUSH string "thumbnailUri" ;
                                      UPDATE ;
                                      SWAP ;
                                      CAR ;
                                      CAR ;
                                      CAR ;
                                      CDR ;
                                      EMPTY_MAP string (pair (option string) string) ;
                                      DUP 2 ;
                                      CDR ;
                                      CDR ;
                                      CDR ;
                                      PUSH string "number" ;
                                      SOME ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Vitality" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CDR ;
                                      CDR ;
                                      CAR ;
                                      PUSH string "number" ;
                                      SOME ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Strength" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CDR ;
                                      CAR ;
                                      CDR ;
                                      NONE string ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Status" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CDR ;
                                      CAR ;
                                      CAR ;
                                      NONE string ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Size" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CDR ;
                                      CDR ;
                                      CDR ;
                                      CDR ;
                                      NONE string ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Secondary personality" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CDR ;
                                      CDR ;
                                      CDR ;
                                      CAR ;
                                      NONE string ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Rarity tier" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CDR ;
                                      CDR ;
                                      CAR ;
                                      CDR ;
                                      PUSH string "number" ;
                                      SOME ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Rarity score" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CDR ;
                                      CDR ;
                                      CAR ;
                                      CAR ;
                                      NONE string ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Primary personality" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CDR ;
                                      CAR ;
                                      CDR ;
                                      CDR ;
                                      PUSH string "number" ;
                                      SOME ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Obedience" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CDR ;
                                      CAR ;
                                      CDR ;
                                      CAR ;
                                      PUSH string "number" ;
                                      SOME ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Intelligence" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CDR ;
                                      CAR ;
                                      CAR ;
                                      CDR ;
                                      NONE string ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Group" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CDR ;
                                      CAR ;
                                      CAR ;
                                      CAR ;
                                      NONE string ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Generation" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CAR ;
                                      CDR ;
                                      CDR ;
                                      CDR ;
                                      NONE string ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Gender" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CAR ;
                                      CDR ;
                                      CDR ;
                                      CAR ;
                                      NONE string ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Fur color" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CAR ;
                                      CDR ;
                                      CAR ;
                                      CDR ;
                                      PUSH string "number" ;
                                      SOME ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Friendliness" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CAR ;
                                      CDR ;
                                      CAR ;
                                      CAR ;
                                      NONE string ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Eyes color" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CAR ;
                                      CAR ;
                                      CDR ;
                                      CDR ;
                                      PUSH string "number" ;
                                      SOME ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Breeding count" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CAR ;
                                      CAR ;
                                      CDR ;
                                      CAR ;
                                      NONE string ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Breed" ;
                                      UPDATE ;
                                      DUP 2 ;
                                      CAR ;
                                      CAR ;
                                      CAR ;
                                      CAR ;
                                      CDR ;
                                      PUSH string "number" ;
                                      SOME ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Bonding level" ;
                                      UPDATE ;
                                      SWAP ;
                                      CAR ;
                                      CAR ;
                                      CAR ;
                                      CAR ;
                                      CAR ;
                                      PUSH string "date" ;
                                      SOME ;
                                      PAIR ;
                                      SOME ;
                                      PUSH string "Birthday" ;
                                      UPDATE ;
                                      PACK ;
                                      SOME ;
                                      PUSH string "attributes" ;
                                      PAIR 3 ;
                                      UNPAIR 3 ;
                                      UPDATE } ;
                                  DUP 5 ;
                                  APPLY ;
                                  DIG 4 ;
                                  DROP ;
                                  PAIR ;
                                  TRANSFER_TOKENS } ;
                              SWAP ;
                              NIL operation ;
                              DIG 2 ;
                              CONS ;
                              PAIR } } } }
               { IF_LEFT
                   { IF_LEFT
                       { DUP 2 ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { DROP 2 ; PUSH string "only controller can call this entrypoint" ; FAILWITH }
                            { LAMBDA
                                (pair (big_map nat nat) nat nat)
                                (big_map nat nat)
                                { UNPAIR ; DUP 2 ; CDR ; SOME ; DIG 2 ; CAR ; UPDATE } ;
                              PUSH nat 1 ;
                              DUP 3 ;
                              CDR ;
                              COMPARE ;
                              EQ ;
                              IF { DUP 3 ;
                                   GET 11 ;
                                   DUP 3 ;
                                   CAR ;
                                   CAR ;
                                   ITER { SWAP ; PAIR ; DUP 2 ; SWAP ; EXEC } ;
                                   SWAP ;
                                   DROP ;
                                   DIG 2 ;
                                   SWAP ;
                                   UPDATE 11 ;
                                   SWAP ;
                                   CAR ;
                                   CDR ;
                                   UPDATE 13 }
                                 { PUSH nat 2 ;
                                   DUP 3 ;
                                   CDR ;
                                   COMPARE ;
                                   EQ ;
                                   IF { DUP 3 ;
                                        GET 15 ;
                                        DUP 3 ;
                                        CAR ;
                                        CAR ;
                                        ITER { SWAP ; PAIR ; DUP 2 ; SWAP ; EXEC } ;
                                        SWAP ;
                                        DROP ;
                                        DIG 2 ;
                                        SWAP ;
                                        UPDATE 15 ;
                                        SWAP ;
                                        CAR ;
                                        CDR ;
                                        UPDATE 17 }
                                      { DROP 3 ; PUSH string "version not found" ; FAILWITH } } ;
                              NIL operation ;
                              PAIR } }
                       { DUP 2 ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { DROP 2 ; PUSH string "only controller can call this entrypoint" ; FAILWITH }
                            { UPDATE 26 ; NIL operation ; PAIR } } }
                   { IF_LEFT
                       { DUP 2 ;
                         GET 5 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { DROP 2 ;
                              PUSH string "only factory can call this entrypoint reveal" ;
                              FAILWITH }
                            { UPDATE 7 ; NIL operation ; PAIR } }
                       { DUP 2 ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { DROP 2 ; PUSH string "only controller can call this entrypoint" ; FAILWITH }
                            { UPDATE 25 ; NIL operation ; PAIR } } } } }
           { IF_LEFT
               { IF_LEFT
                   { DUP 2 ;
                     GET 5 ;
                     SENDER ;
                     COMPARE ;
                     NEQ ;
                     IF { DROP 2 ; PUSH string "only factory can call this entrypoint" ; FAILWITH }
                        { UPDATE 1 ; NIL operation ; PAIR } }
                   { DUP 2 ;
                     GET 3 ;
                     SENDER ;
                     COMPARE ;
                     NEQ ;
                     IF { DROP 2 ; PUSH string "only controller can call this entrypoint" ; FAILWITH }
                        { DUP 2 ;
                          GET 25 ;
                          SWAP ;
                          VIEW "getRandomBetween" nat ;
                          IF_NONE
                            { PUSH string "Oracle failed to supply random number" ; FAILWITH }
                            {} ;
                          UPDATE 21 ;
                          NIL operation ;
                          PAIR } } }
               { DUP 2 ;
                 GET 3 ;
                 SENDER ;
                 COMPARE ;
                 NEQ ;
                 IF { DROP 2 ; PUSH string "only controller can call this entrypoint" ; FAILWITH }
                    { DUP 2 ;
                      GET 19 ;
                      SWAP ;
                      ITER { SWAP ; DUP 2 ; CDR ; SOME ; DIG 2 ; CAR ; UPDATE } ;
                      UPDATE 19 ;
                      NIL operation ;
                      PAIR } } } } }

