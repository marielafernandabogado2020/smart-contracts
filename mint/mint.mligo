#if !MINT
#define MINT
#include "./mint_interface.mligo"
#include "./mint_constants.mligo"

[@inline]
let token_mint (nft_param : nft_mint_param) (store: storage) : operation =
    match (Tezos.get_entrypoint_opt "%mint" store.nft_address : nft_mint_param contract option) with
    | None -> (failwith error_NFT_CONTRACT_MUST_HAVE_A_MINT_ENTRYPOINT : operation)
    | Some token_mint_entrypoint -> Tezos.transaction nft_param 0mutez token_mint_entrypoint

[@inline]
let token_burn (param : burn_param) (store: storage) : operation =
    match (Tezos.get_entrypoint_opt "%burn" store.nft_address : burn_param contract option) with
    | None -> (failwith error_NFT_CONTRACT_MUST_HAVE_A_BURN_ENTRYPOINT : operation)
    | Some token_burn_entrypoint -> Tezos.transaction param 0mutez token_burn_entrypoint

[@inline]
let xtz_transfer (to_ : address) (amount_ : tez) : operation =
    match (Tezos.get_contract_opt to_ : unit contract option) with
    | None -> (failwith error_INVALID_TO_ADDRESS : operation)
    | Some to_contract -> Tezos.transaction () amount_ to_contract

[@inline]
let fa12_transfer (fa12_address : address) (from_ : address) (to_ : address) (value : nat) : operation =
  let fa12_contract : fa12_contract_transfer contract =
    match (Tezos.get_entrypoint_opt "%transfer" fa12_address : fa12_contract_transfer contract option) with
    | None -> (failwith("FA1.2 contract must have a transfer entrypoint") : fa12_contract_transfer contract)
    | Some contract -> contract in
    let transfer = {address_from = from_; address_to = to_; value = value} in
    Tezos.transaction transfer 0mutez fa12_contract

let set_pause (param : set_pause_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.factory then
    (failwith("only factory can call this entrypoint") : return)
  else
    ([] : operation list), { store with paused = param }

let set_nft_address (new_nft_address: set_nft_address_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.factory then
    (failwith("only factory can call this entrypoint mint") : return)
  else
    ([] : operation list), { store with nft_address = new_nft_address }

let set_doga_address (new_doga_address: set_doga_address_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
    ([] : operation list), { store with doga_address = new_doga_address }

let set_reserve_address (new_reserve_address: set_reserve_address_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
  ([] : operation list), { store with reserve_address = new_reserve_address }

let set_mint_params (param: set_mint_params_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
    if param.mint_price <= 0n || param.total_supply = 0n || param.max_mint_per_address = 0n then
      (failwith error_VALUE_SHOULD_BE_HIGHER_THAN_ZERO : return)
    else
      if param.start_time >= param.end_time then
        (failwith error_INVALID_DATE_FORMAT : return)
      else
        if param.currency <> "XTZ" && param.currency <> "FA12" then
          (failwith error_INVALID_CURRENCY : return)
        else
          let new_store = { store with
            is_private_sale = param.is_private_sale;
            mint_price = param.mint_price;
            currency = param.currency;
            next_token_id = param.next_token_id;
            total_supply = param.total_supply;
            max_mint_per_address = param.max_mint_per_address;
            token_per_pack = param.token_per_pack;
            start_time = param.start_time;
            end_time = param.end_time;
          } in
          (([] : operation list), new_store)

let set_mint_per_address (store : storage) : return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
    ([] : operation list), { store with mint_per_address = (Big_map.empty : (address, nat) big_map) }

let add_to_whitelist (param: add_to_whitelist_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
    let add_addresses (wl, addr : whitelist * (address * bool)) : whitelist = Big_map.update addr.0 (Some addr.1) wl in
    let updated_whitelist = List.fold add_addresses param store.whitelist in
    ([] : operation list), { store with whitelist = updated_whitelist }

let remove_from_whitelist (param: remove_from_whitelist_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
    let remove_addresses (wl, addr : whitelist * address) : whitelist = Big_map.update addr (None: bool option) wl in
    let updated_whitelist = List.fold remove_addresses param store.whitelist in
    ([] : operation list), { store with whitelist = updated_whitelist }

let set_default_metadata (param: set_default_metadata_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
    let add_metadatas (dm, meta : token_metadata * (string * bytes)) : token_metadata = Map.update meta.0 (Some meta.1) dm in
    let updated_metadata = Map.fold add_metadatas param store.default_metadata in
    ([] : operation list), { store with default_metadata = updated_metadata }

let get_default_metadata () (store: storage) : token_metadata =
  let name = match Big_map.find_opt store.next_token_id store.name_list with
    | Some name -> name
    | None -> ("446f67616d69" : bytes) // Dogami
  in
  Map.update "name" (Some name) store.default_metadata

let get_price (token_count : nat) (store : storage) : nat =
  if store.token_per_pack <> 0n then
    if token_count mod store.token_per_pack <> 0n then
      (failwith error_INVALID_NUMBER_OF_TOKENS : nat)
    else
      token_count * store.mint_price / store.token_per_pack
  else
    token_count * store.mint_price

// need to allow tokens before calling this entrypoint
let mint_from_crowdsale (param: mint_from_crowdsale_param) (storage: storage): return =
  if (param.token_count < 1n) then
    (failwith error_VALUE_SHOULD_BE_HIGHER_THAN_ZERO : return)
  else
    if storage.paused = true then
      (failwith error_CONTRACT_IN_PAUSE : return)
    else
      if (Tezos.get_now ()) < storage.start_time || (Tezos.get_now ()) >= storage.end_time then
        (failwith error_MINT_PERIOD_IS_NOT_OPEN : return)
      else
        let _whitelisted =
          if storage.is_private_sale then
            match Big_map.find_opt param.to_ storage.whitelist with
              | None -> (failwith error_ADDRESS_IS_NOT_WHITELISTED : bool)
              | Some b -> b
          else
            False
        in
        let price = get_price param.token_count storage in
        let transfer_op =
          if storage.currency = "XTZ" then
            [xtz_transfer storage.reserve_address (price * 1mutez)]
          else
            [fa12_transfer storage.doga_address (Tezos.get_sender ()) storage.reserve_address price]
        in

        (* single mint *)
        let rec single_mint (acc_token_count, acc: nat * acc) : return =
          if (acc_token_count < 1n) then
            acc.operation_list, acc.storage
          else
            if acc.storage.next_token_id > storage.total_supply then
              (failwith error_TOTAL_SUPPLY_REACHED : return)
            else
              let minted_per_address = match Big_map.find_opt param.to_ acc.storage.mint_per_address with
                | None -> 0n
                | Some n ->
                  if (n >= storage.max_mint_per_address)
                    then (failwith error_MAX_MINT_PER_ADDRESS_REACHED : nat)
                  else
                    n
              in
              let token_id = acc.storage.next_token_id in
              let token_info = get_default_metadata () acc.storage in
              let new_name_list = Big_map.update acc.storage.next_token_id (None: bytes option) acc.storage.name_list in

              let op_mint = token_mint { to_ = param.to_; token_id = token_id; token_info = token_info } acc.storage in
              let ops = op_mint :: acc.operation_list in

              let new_mint_per_address = Big_map.update param.to_ (Some (minted_per_address + 1n)) acc.storage.mint_per_address in
              let new_next_token_id = acc.storage.next_token_id + 1n in
              let new_acc = {
                operation_list = ops;
                storage = {
                  acc.storage with
                  name_list = new_name_list;
                  next_token_id = new_next_token_id;
                  mint_per_address = new_mint_per_address
                }
              }
              in
              match is_nat (acc_token_count - 1n) with
                | None -> (failwith error_INVALID_NAT_VALUE : return)
                | Some n -> single_mint (n, new_acc)
        in
        let accumulator = {
          operation_list = transfer_op;
          storage = storage;
        } in
        single_mint (param.token_count, accumulator)

(* need operator permission to use this entrypoint *)
let burn_from_crowdsale (param: burn_from_crowdsale_param) (store: storage): return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
    if store.paused = true then
      (failwith error_CONTRACT_IN_PAUSE : return)
    else
      let op = token_burn param store in
      ([op], store)

let set_name_list (param : set_name_list_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
    let add_name (acc, el: name_list * (nat * bytes)) : name_list = Big_map.update el.0 (Some el.1) acc in
    let new_name_list = Map.fold add_name param store.name_list in
    ([] : operation list), { store with name_list = new_name_list }

let main (action, store : parameter * storage) : return =
  match action with
    | SetPause p -> set_pause p store
    | SetNftAddress p -> set_nft_address p store
    | SetDogaAddress p -> set_doga_address p store
    | SetReserveAddress p -> set_reserve_address p store
    | SetMintParams p -> set_mint_params p store
    | SetMintPerAddress -> set_mint_per_address store
    | AddToWhitelist p -> add_to_whitelist p store
    | RemoveFromWhitelist p -> remove_from_whitelist p store
    | SetDefaultMetadata p -> set_default_metadata p store
    | MintFromCrowdsale p -> mint_from_crowdsale p store
    | BurnFromCrowdsale p -> burn_from_crowdsale p store
    | SetNameList p -> set_name_list p store
#endif