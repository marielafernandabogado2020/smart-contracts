# Dogami Contracts

## User Instructions

### run sandbox

From the root folder, run `make start-sandbox`. This will run a local sandbox node to enable testing the contracts.

To stop the sandbox, run `make kill-sandbox`.

### compile

From the root folder, run `make compile`. This will compile all contracts with ligo version `0.46.1`

### test

From the root folder, run `make test-CONTRACTNAME`
CONTRACTNAME is one of the following:
- factory
- marketplace

This will run unit tests for all features of the desired contract.

### deploy

From the root folder, run `make deploy network=NETWORK`
NETWORK is one of the following:
- sandbox
- testnet
- mainnet

If you choose mainnet as the network, you will be asked to supply your `public key hash` and your `secret key`.

# Dogami factory contract

**This is a contract controlling all aspects of the crowdsale operations for dogami**

---

## Contracts architecture

The factory structure deploys sets of three different contracts (minter, revealer, nft), constructing the "crowdsale" structure for Dogami.

The architecture is built to maximize flexibility of the contract's storages and their interface, allowing to deploy different contracts versions.

The factory is made up of 7 different contracts:

- **Factory** contract: This contract is used to manage the deployment of the contract sets and save the contract addresses.
It also manages the architecture of the different `controller` and `launcher` contracts.
- **Launcher** contracts: The launcher contracts deploy the different contracts. This structure is used to enable flexible contracts - if the structure of either of the deployed contract's storage changes, only the launcher contract for this contract type needs to be re-deployed.
- - The **mint launcher** is responsible for the **minter** contract deployment and initial storage assignment.
- - The **reveal launcher** is responsible for the **revealer** contract deployment and initial storage assignment.
- - The **NFT launcher** is responsible for the **NFT** (FA2) contract deployment and initial storage assignment.

- **controller** contracts: The controller contracts control all admin entrypoints of the different contracts. A single multisig contract is used by all controller contracts, and all admin entrypoints in the contract sets are called through the controllers.
The controllers were also written to enable maximal flexibility of the different contracts. If a new interface is needed to either or all contracts in a set, only the controllers need to be re-deployed.

**Some key values of contracts are controlled directly through the factory contract and not through the controller contracts, to allow control of multiple contracts sharing the same value**

### Structure diagram

```mermaid
flowchart TD
A(Factory)
B(Mint Launcher)
C(Reveal Launcher)
D(NFT Launcher)
E(Mint Controller)
F(Reveal Controller)
G(NFT Controller)
H(Minter)
I(Revealer)
J(NFT)
K(Multisig)
A-->|set_id|K
K-->B
K-->C
K-->D
K-->E
K-->F
K-->G
B-->|Storage|H
E-->|Interface|H
C-->|Storage|I
F-->|Interface|I
D-->|Storage|J
G-->|Interface|J
subgraph Contract set
H
I
J
end
```

## Factory entrypoints

### deploySet
Deploy a contract set with an NFT contract, a minter contract and a revealer contract.
Contract sets are saved in storage, including the individual contract's addresses for each set.
This entrypoint uses a series of callbacks to the `launcher` contracts, to deploy the three different contracts. This requires using a variant type for the entrypoint call. The only variant accessible by the user is the `deploy` type.

**Input parameter:**
- `deploy of string` : the `set_id` (set name) of the new deployed set

### updateMultisig
Update the multisig contract's address.
This entrypoint updates also the controller contract's multisig.

**Input parameter:**
- `address` : the new multisig's address

### setPause
Pause or unpause a contract's operations.
This entrypoint can set contracts in different manners:
- A single contract is paused/unpaused
- Both contracts (minter and revealer) for a single set are paused/unpaused
- A single contract (either minter or revealer) is paused over multiple sets
- Both contracts are paused/unpaused over multiple sets

**Input parameters:**
- update_method :
```ocaml
| single of set_id
| multiple of set_id list
```
- choose_contract :
```ocaml
| minter
| revealer
| both
```
- `pause : bool` : either true (pause) or false (unpause)

### setNftAddress
Change the chosen set's NFT contract address

**Input parameters:**
- `set_id : string` : the id of the chosen set to update
- `nft_address : address` : the new NFT contract's address

### setDogaAddress
Update the factory's default doga address.
This address will then be assigned to the minter contract on deployment.

**Input parameters:**
- `address` : the new default doga's address

### setOracleAddress
Update the factory's default oracle address.
This address will then be assigned to the revealer contract on deployment.

**Input parameters:**
- `address` : the new default oracle's address

### setDefaultReserve
Update the factory's default reserve address.
This address will then be assigned to the minter contract on deployment.

**Input parameters:**
- `address` : the new default reserve's address

### setRevealerAddress
Change the chosen set's revealer contract address

**Input parameters:**
- `set_id : string` : the id of the chosen set to update
- `revealer_address : address` : the new revealer contract's address

### setMinterAddress
Change the chosen set's minter contract address

**Input parameters:**
- `set_id : string` : the id of the chosen set to update
- `minter_address : address` : the new minter contract's address

### updateLaunchers
Update each of the factory's launchers addresses.
The purpose of the launchers contracts is to deploy the different contracts with their initial storage.

**Input parameters:**
- `mint_launcher` : the new mint launcher contract's address
- `reveal_launcher` : the new reveal launcher contract's address
- `nft_launcher` : the new nft launcher contract's address

### updateControllers
Update each of the factory's controllers addresses.
The purpose of the controller contracts is to call the admin entrypoints of the different contracts.

**Input parameters:**
- `mint_controller` : the new mint controller contract's address
- `reveal_controller` : the new reveal controller contract's address
- `nft_controller` : the new nft controller contract's address


# Dogami Nft contract

**This is the contract storing the Dogami's Nfts**

---

## Nft public entrypoints

### Mint
This entrypoint is used to create a new nft with its token_id, owner and metadatas

**input parameters:**
- `token_info: (string, bytes) map` : a map of metadatas
- `token_id: nat` : the new token id
- `to_: address` : the address of the owner

**example:**
- `nft.mint({"to_": ALICE_KEY, "token_id": 14, "token_info": {}})`

**requirements:**
- The entrypoint must not be in pause
- Only an authorized contract can call this entrypoint
- The `token_id` must not already exists

**storage update:**
- The token_id and owner address are set in `nft.storage.ledger`
- The token_id and token_info are set in `nft.storage.token_metadata`
- `nft.storage.total_supply` is increased by one

### Transfer
This entrypoint is used to transfer the ownership of a Nft from an address to another

**input parameters:**
- `transfer list`
```ocaml
type transfer_destination =
{
  to_ : address;
  token_id : token_id;
  amount : nat;
}
type transfer =
{
  from_ : address;
  txs : transfer_destination list;
}
```

**example:**
- `nft.transfer([{"from_": ALICE_KEY, "txs": [{"amount": 1, "to_": BOB_KEY, "token_id": 14}]}]`

**requirements:**
- The `token_id` must exists
- Each sender must be allowed to make the transfer

**storage update:**
- The receiver address is updated as the new owner in `nft.storage.ledger`

### Burn
This entrypoint is used to burn a nft

**input parameters:**
- `token_id : nat` : the id of the token to burn
- `from_ : address` : the address of the owner of the token

**example:**
- `nft.burn({"token_id": 14, "from_": ALICE_KEY})`

**requirements:**
- The entrypoint must not be in pause
- Only an authorized contract can call this entrypoint
- The address must match the owner's address
- The caller must be allowed to call this entrypoint

**storage update:**
- The token_id and address are removed from the big map `nft.storage.ledger`
- The token_id and token_metadata are removed from the big map `storage.token_metadata`
- `storage.total_supply` is decreased by one

### Balance_of
This entrypoint is used to get the balance of tokens for an address

**input parameters:**
- `balance_of_param`
```ocaml
type balance_of_request =
{
  owner : address;
  token_id : token_id;
}
type balance_of_response =
{
  request : balance_of_request;
  balance : nat;
}
type balance_of_param =
{
  requests : balance_of_request list;
  callback : (balance_of_response list) contract;
}
```

**example:**
- `nft.balance_of({ "requests": [{"owner": ALICE_KEY, "token_id": token_id}], "callback": contract_entrypoint})`

### Balance_of_view
This entrypoint is used as an alternative of the balance_of entrypoint to get the balance of tokens for an address

**input parameters:**
- `token_id: nat` the id of the token
- `owner: address` the address of the owner

**example:**
- `Tezos.call_view "balance_of_view" { token_id = 14; owner = ALICE_KEY } nft_address`

**return value:**
- `1n` if the address is the owner of the nft, else `0n`

### Total_supply_view
This entrypoint is used to get the total_supply value from the storage of the nft contract

**input parameters:**
- `unit` : this entrypoint requires no parameter

**example:**
- `Tezos.call_view "total_supply_view" () nft_address`

**return value:**
- returns `nft.storage.total_supply`

### Update_operators
This entrypoint is used to give or retrieve allowance to an other address to make operation on his token

**input parameters:**
```ocaml
type operator_param =
{
  owner : address;
  operator : address;
  token_id: token_id;
}
type update_operator =
  | Add_operator of operator_param
  | Remove_operator of operator_param
```

**example:**
- `nft.update_operators([{"add_operator": {"owner": ALICE_KEY, "operator": BOB_KEY, "token_id": 12}}])`

**requirements:**
- The caller must be the owner of the token

**storage update:**
- The address of the owner, the address of the validator and the token id are set in `nft.storage.operator_storage`

### UpdateMetadataWithFunction
This entrypoint is used to update the metadatas of a Nft

**input parameters:**
- `token_id: nat` : the token id of the metadata to update
- `metadata_updater: function` a function to apply onto the current metadata

**requirements:**
- Only a contract listed in `nft.storage.contracts` can call this entrypoint

**storage update:**
- The metadata in `nft.storage.token_metadata` is updated

## Nft entrypoints managed by the nft controller contract

### SetMetadata
This entrypoint is used to update the metadata of the nft contract

**input parameters:**
- `set_metadata_param : bytes` : the new metadata content
- `set_id : string` : the id of the chosen set to update

**requirements:**
- The caller must be the nft controller contract

**storage update:**
- The new metadata value is set in `nft.storage.metadata` with the key `content`

### SetBurnPause
This entrypoint is used to pause the Burn entrypoint

**input parameters:**
- `set_burn_pause_param : bool` : either true (pause) or false (unpause)
- `set_id : string` : the id of the chosen set to update

**requirements:**
- The caller must be the nft controller contract

**storage update:**
- The boolean value is set in `nft.storage.burn_paused`

### SetContractPause
This entrypoint is used to pause the nft contract's following entrypoints:
- `transfer`
- `mint`

**input parameters:**
- `set_contract_pause_param : bool` : either true (pause) or false (unpause)
- `set_id : string` : the id of the chosen set to update

**requirements:**
- The caller must be the nft controller contract

**storage update:**
- The boolean value is set in `nft.storage.contract_paused`

### AddContract
This entrypoint is used to add an address to a set of externals contracts addresses allowed to call the following entrypoints:
- Mint
- Burn
- UpdateMetadataWithFunction

**input parameters:**
- `add_contract_param : address` : the contract address to add
- `set_id : string` : the id of the chosen set to update

**requirements:**
- The caller must be the nft controller contract

**storage update:**
- The address is added to the set of addresses `nft.storage.contracts`

### RemoveContract
This entrypoint is used to remove an address from the set of externals contracts addresses allowed to call the following entrypoints:
- Mint
- Burn
- UpdateMetadataWithFunction

**input parameters:**
- `remove_contract_param : address` : the contract address to remove
- `set_id : string` : the id of the chosen set to update

**requirements:**
- The caller must be the nft controller contract

**storage update:**
- The address is removed from the set of addresses `nft.storage.contracts`

## Nft controller entrypoints

### UpdateMultisig
This entrypoint is used to update the address of the multisig in the nft controller contract

**input parameters:**
- `address` : the new multisig contract address

**requirements:**
- The caller must be the factory contract

**storage update:**
- The new address is set in `nft_controller.storage.multisig`

### UpdateFactory
This entrypoint is used to update the address of the factory in the nft controller contract

**input parameters:**
- `address` : the new factory contract address

**example:**
- `nft_controller.updateFactory('KT1FDSvLCUcFZStQAKo5Fb4igCwtqipRcvMX')`

**requirements:**
- The caller must be an admin address registered in the multisig contract

**storage update:**
- The new address is set in `nft_controller.storage.factory`


# Dogami Mint contract

**This is the contract used to mint new nfts, admin entrypoints are to be called either from the factory contract or the mint controller contract**

---

## Mint entrypoints managed by the factory contract

### setPause
Enables or disables `mintFromCrowdsale` and `burnFromCrowdsale` entrypoints

### setNftAddress
Updates the nft contract's address

## Mint public entrypoint

### mintFromCrowdsale
Mint a a new token on the nft contract.
Conditions for the mint are set in `setMintParams`, for xtz payment the amount have to be send along with the parameters,
for doga payment the amount have to be previously approved from the Doga contract.

**Input parameters:**
- `token_count` : the number of nft to mint
- `to_` : the address of the owner of the nft

## Mint entrypoints managed by the mint controller contract

### setDogaAddress
Change the address of the DOGA payment token.
This entrypoint can be used to set the DOGA address for either a single set's minter, or for multiple sets.
The default address of DOGA can also be set for future set deployments.

**Input parameters:**
- update_method :
```ocaml
| single of set_id
| multiple of set_id list
```
- `doga_address : address` : The new address for the DOGA token.
- `update_self : bool` : update the default DOGA address or not.

### setReserveAddress
Change the address receiving the fees and payments from the mint contract.
This entrypoint can be used to set the reserve address for either a single set's minter, or for multiple sets.
The default address of reserve can also be set for future set deployments.

**Input parameters:**
- update_method :
```ocaml
{
| single of set_id
| multiple of set_id list
}
```
- `reserve_address : address` : The new address for the reserve
- `update_self : bool` : update the default reserve address or not

### setMintParams
Set mint parameters for a minter contract

**Input parameters:**
- `set_id : string` : the id of the chosen set to update
- set_mint_params :
```ocaml
{
    is_private_sale : bool
    mint_price : nat
    currency : string
    next_token_id : nat
    total_supply : nat
    max_mint_per_address : nat
    token_per_pack : nat
    start_time : timestamp
    end_time : timestamp
}
```

### setMintPerAddress
Revert a minter contract's `mint_per_address` to an empty `big_map`

**Input parameters:**
- `set_id : string` : the id of the chosen set to update

### addToWhitelist
Add a list of addresses to a minter contract's whitelist

**Input parameters:**
- `set_id : string` : the id of the chosen set to update
- `add_to_whitelist_params : (address * bool) list` : a list of addresses to add to the whitelist. The boolean value is irrelevant but needs to be specified

### removeFromWhitelist
Remove a list of addresses from a minter contract's whitelist

**Input parameters:**
- `set_id : string` : the id of the chosen set to update
- `remove_from_whitelist_params : address list` : a list of addresses to remove from the whitelist

### setDefaultMetadata
Set a minter contract's default token metadata

**Input parameters:**
- `set_id : string` : the id of the chosen set to update
- `set_default_metadata_param : (string * bytes) map` : the new default metadata.

### burnFromCrowdsale
Burn an NFT from minted by a minter contract

**Input parameters:**
- `set_id : string` : the id of the chosen set to update
- burn_from_crowdsale_param :
```ocaml
{
    token_id : nat
    from_ : address
}
```

### setNameList
Add the list of names for each token id, which will be assigned at each new metadata.
All the names are in hexadecimal and could be not be converted directly on chain.

**Input parameters:**
- `(nat, bytes) map` : a map of each id and their value in hexadecimal

## Mint controller entrypoints

### updateMultisig
Updates the mint controller contract's multisig address

**Input parameters:**
- `address` : the new multisig's address

### updateFactory
Updates the mint controller contract's factory address

**Input parameters:**
- `address` : the new factory's address


# Dogami Reveal contract

**This is the contract used to reveal nfts, meaning to assign a metadata id to a nft and sending the new metadata to the nft contract**

---

## Reveal entrypoints managed by the factory contract

### setPause
Enables or disables the `assignMetadata` entrypoint

### setNftAddress
Updates the nft contract's address

## Reveal public entrypoint

### assignMetadata
This entrypoint is used to obtain a random metadata id for the token sent as parameter.
Only the owner of the token can call this entrypoint.
The metadata id will be selected from the list of free metadatas available in the storage.

**input parameters:**
- `nat` : the token id who will be assigned a metadata id

### reveal
This entrypoint will be used to call the `updateMetadataWithFunction` entrypoint in the nft contract.
It can only be called by an admin listed in the storage `reveal_admins`.
The attributes parameters are converted in the contract in the function `attributes_from_param`. This was decided to reduce the parameter size of the call to the entrypoint.

**input parameters:**
- reveal_param :
```ocaml
type attributes_from_param_param =
{
  a: string;
  b: string;
  c: string;
  d: string;
  e: string;
  f: string;
  g: string;
  h: string;
  i: string;
  j: string;
  k: string;
  l: string;
  m: string;
  n: string;
  o: string;
  p: string;
  q: string;
  r: string;
  s: string;
  t: string;
}
type reveal_param_inner =
{
  artifactUri: bytes;
  attributes: attributes_from_param_param;
  displayUri: bytes;
  formats: bytes;
  thumbnailUri: bytes;
  metadata_id: nat;
}
type reveal_param =
{
  metadata: reveal_param_inner;
  token_id: token_id;
}
```

## Reveal entrypoints managed by the reveal controller contract

### setOracleAddress
Update the reveal's randomizer contract address.
This randomizer contract is called at the beginning of the reveal phase to retrieve a random number.

**Input parameters:**
- `set_oracle_address_param : address` : the address of the randomizer contract
- `set_id : string` : the id of the chosen set to update

### setFreeMetadata
Set a list of free metadata ids in the storage.
These metadata ids will be available to be assigned in the `assignMetadata` entrypoint.
The reveal contract is able to handle 2 drops of nfts, for dogami the drop 1 corresponds to the token 1 to 8000, and the drop 2 is going from 8001 to 12000.

**Input parameters:**
- `set_id : string` : the id of the chosen set to update
- `set_free_metadata_param : <type set_free_metadata_param>` : the list of free metadata ids, the length of the list, and the drop id corresponding to these metadatas
```ocaml
type set_free_metadata_param = {
  version: nat;
  free_metadata: (nat, metadata_id) map;
  free_metadata_length: nat;
}
```

### setTokenIdToMetadataId
This entrypoint is used to update the metadata id corresponding to the token id.
It was used to migrate the data from a previous reveal contract.

**Input parameters:**
- `set_token_id_to_metadata_id_param : (token_id, metadata_id) map` : a map of token ids and their related metadata ids
- `set_id : string` : the id of the chosen set to update

### setRandomOffset
This entrypoint is used at the beginning of the reveal phase to get a random number before the first call of `assignMetadata`.
This random number is obtained by calling the randomizer contract.

**Input parameters:**
- `set_id : string` : the id of the chosen set to update
- `set_random_offset_param : <type set_random_offset_param>` : a range of nat to delimit the random number
```ocaml
type set_random_offset_param =  //nat
{
    from_: nat;
    to_: nat;
}
```

### addRevealAdmin
This entrypoint is used to add a reveal admin to the reveal contract storage.
Multiple reveal admins are required to allow many calls at the entrypoint at the same time, and thus to reveal many tokens simultaneously.

**Input parameters:**
- `add_reveal_admin_param : address` : the address of the admin to add
- `set_id : string` : the id of the chosen set to update

### removeRevealAdmin
This entrypoint is used to remove a reveal admin from the reveal contract storage.

**Input parameters:**
- `remove_reveal_admin_param : address` : the address of the admin to remove
- `set_id : string` : the id of the chosen set to update

### setIpfsHashes
This entrypoint is register an ipfs hash linking to a file containing all the metadata ids and a hash of each metadata content.
This allows to verify the metadata content has not changed before and after the call to the `assingMetadata` entrypoint.

**Input parameters:**
- `set_ipfs_hashes_param : string` : the hash of the ipfs file
- `set_id : string` : the id of the chosen set to update

## Reveal controller entrypoints

### updateMultisig
Updates the reveal controller contract's multisig address

**Input parameters:**
- `address` : the new multisig's address

### updateFactory
Updates the reveal controller contract's factory address

**Input parameters:**
- `address` : the new factory's address


# Dogami Marketplace contract

## Operations:

---

* A token is minted by the asset owner.
* The minted token is added to the marketplace by the owner.
* The buyer sends payment, if the corect amount is sent the buyer gets ownership of the token, and the former owner gets funds.

**Note: For all swap types, royalties and management fees are deducted from the swap owner's funds at the swap finalisation**

## Administration Tools:

---

The marketplace contract is controlled by a multisig contract, with the ability to change some key configurations with the following entrypoints:

### **setPause**

Pause some of the marketplace's entrypoints.
The disabled entrypoints when the marketplace is paused are:
- `addToMarketplace`
- `updateSwap`
- `collect`
- `sendOffer`
- `updateOffer`
- `makeCounterOffer`
- `acceptCounterOffer`
- `acceptOffer`

### **addCollection:**

Add an authorized NFT contract address to the marketplace's `collections`.

**Input parameter:**
- `address`: The new NFT contract address.

### **removeCollection**

Remove an authorized NFT contract address from the marketplace's `collections`.

**Input parameter:**
- `address`: The new NFT contract address.

### **updateRoyaltiesAddress**

Update the address used to collect royalties fee.

**Input parameter:**
- `address`: The new royalties address.

### **updateTreasuryAddress**

Update the address used to collect the platform fees.

**Input parameter:**
- `address`: The new treasury address.

### **updateFee:**

Change the management fee. The fee applies to all swaps under the contract.
**Input parameter:**
- `nat`: The new management fee.

**note: the management fee applied to swaps is specified by `transaction_amount * management_fee_rate / 10,000`, or more simply `management_fee_rate * 0.01%`, to allow two decimals.**

### **updateOracleAddress**

Update the oracle address used by the marketplace to make currency convertions.
The interface of the oracle contract should be similar to the interface of the Harbinger `normalizer` contract.

**Input parameter:**
- `address`: The new oracle contract's address.

### **updateRoyalties**

Update the marketplace's global `royalties_rate`.

**Input parameter:**
- `nat`: The new royalties rate

**note: the royalties fee applied to swaps is specified by `transaction_amount * royalties_rate / 10,000`, or more simply `royalties_rate * 0.01%`, to allow two decimals.**

### **updateAllowedTokens**

Update the liist of tokens supported by the marketplace for currency conversion.
This entrypoint update the marketplace storage's `allowed_tokens` and `available_pairs`.

**Input parameters:**
***add token:***
- `token_symbol : string` : the symbol of the token. This symbol should be identical to the token symbol in the Harbinger contract.
-
```ocaml
direction = add_token :
      {
        fa_address : address;
        fa_type : string;
      }
```

***remove token:***
- `token_symbol : string` : the symbol of the token. This symbol should be identical to the token symbol in the Harbinger contract.
= `direction = add_token : unit`

### **setOracleTolerance**

Set tolerance for intervals between oracle updates. If the oracle doesn't update for a longer interval than this tolerance, conversion using the oracle is considered inaccurate, and currency conversions are disabled.

**Input parameter:**

- `nat` : the tolerance interval in seconds.

### **updateMultisig**
Update the multisig contract's address.

**Input parameter:**
- `address` : the new multisig's address.

## Seller Tools:
---

### ***IMPORTANT : before initialising a swap using `addToMarketplace`, the calling contract has to be confirmed as an operator by the token owner. `nft.updateOperator` entrypoint has to be called first.***

---
### **addToMarketplace**

This entry-point is used for initializing a fixed-price type swap.

**Input parameters:**
- `swap_type : swap_type` : either `regular` fixed-price sale or `dutch` (dutch auction).
- `token_id: nat` : The token which is being put on sale.
- `token_origin : address` : the collection to which the added token belongs to.
- `token_price: nat` : The token's price in the chosen payment token.
- `start_time: timestamp` : The time at which the token will be put up for sale.
- `end_time: timestamp` : The time at which the token will not be on sale anymore.
- `recepient : recepient_type` : either reserved buyer or general.
- `token_symbol : string` : the symbol of the payment token.
- `accepted_tokens : string set` : the payment tokens accepted by the seller.
- `is_multi_token : bool` : if false, only `XTZ` or `DOGA` are available for payment, and no currency conversion is available.

### **removeFromMarketplace**
Remove a token from the marketplace.

**Input parameter:**
- `nat` : the swap id to remove.

### **collect**
Pay for a token that was put up for sale on the marketplace and obtain ownership of the token.

**Input parameters:**
- `swap_id : nat` : the id of the swap to collect.
- `token_symbol : string` : the chosen payment token. If the token symbol is `XTZ`, XTZ amount should be sent with the transaction.
- `amount_ft : nat` : the amount of payment tokens to transfer. if `token_symbol = "XTZ"`, this should be the same value as the XTZ amount sent with the transaction.
- `to_ : address` : the address of the account receiving the bought NFT.

### **sendOffer**
Send a swap offer for an NFT. The NFT doesn't necessarily exist on the marketplace existing swaps, but has to be a part of a collection supported by the marketplace.

**Input parameters:**
- `token_id : nat` : the id of the wanted token.
- `token_origin : address` : the address of the collection to which the NFT belongs.
- `start_time : timestamp` : the time from which the offer takes effect.
- `end_time : timestamp` : the time after which the offer is no longer available.
- `token_symbol : string` : the symbol of the offer's payment token.
- `ft_amount : nat` : the amount of payment tokens to transfer. if `token_symbol = "XTZ"`, this should be the same value as the XTZ amount sent with the transaction.

### **updateOffer**
Update an existing offer's parameters.

**Input parameters:**
- `token_id : nat` : the id of the wanted token.
- `token_origin : address` : the address of the collection to which the NFT belongs.
- `start_time : timestamp` : the time from which the offer takes effect.
- `end_time : timestamp` : the time after which the offer is no longer available.
- `token_symbol : string` : the symbol of the offer's payment token.
- `ft_amount : nat` : the amount of payment tokens to transfer. if `token_symbol = "XTZ"`, this should be the same value as the XTZ amount sent with the transaction.

### **withdrawOffer**
Cancel an existing offer.

**Input parameters:**
- `token_id : nat` : the id of the canceled offer's NFT.
- `token_origin : address` : the address of the collection to which the NFT belongs.

### **makeCounterOffer**
Make a counter offer for an existing offer by an owner of the NFT for which the offer was made.

**Input parameters:**
- `token_id : nat` : the id of the NFT for which the original offer was made.
- `token_origin : address` : the address of the collection to which the NFT belongs.
- `buyer : address` : the address of the original offer maker.
- `start_time : timestamp` : the time from which the counter offer takes effect.
- `end_time : timestamp` : the time after which the counter offer is no longer available.
- `token_symbol : string` : the symbol of the counter offer's accepted payment token.
- `ft_amount : nat` : the new price asked by the counter offer's maker.

### **withdrawCounterOffer**
Cancel an existing counter offer.

**Input parameters:**
- `token_id : nat` : the id of the NFT for which the counter offer was made.
- `buyer : address` : the address of the original offer maker.
- `token_origin : address` : the address of the collection to which the NFT belongs.

### **acceptCounterOffer**
Accept a counter offer.

**Input parameters:**
- `token_id : nat` : the id of the NFT for which the counter offer was made.
- `token_origin : address` : the address of the collection to which the NFT belongs.
- `seller : address` : the address of the accepted counter offer's maker.

### **acceptOffer**
Accept an offer.

**Input parameters:**
- `buyer : address` : the address of the offer maker.
- `token_id : nat` : the id of the NFT for which the offer was made.
- `token_origin : address` : the address of the collection to which the NFT belongs.

### **updateSwap**

Change a swap existing on the marketplace.

**Input parameters:**
- `swap_id : nat` : The updated swap.
-
``` ocaml
action : action =
| Update_price of nat : the new price of the NFT
| Update_times of {
  start_time : timestamp : the new swap's start
  end_time : timestamp : the new swap's end
}
| Update_reserved_address of address : the new swap's recepient
| Update_duration of nat : the new duration for dutch auction
| Update_starting_price of nat : the new starting price for dutch auction
```
---
## Cross-Contract Interactions:

The marketplace contract interacts with multiple `FA2` NFT contract.
Interactions are made at a few different stages of operation:

**swap initialisation:**

The NFT's contract's `transfer` entrypoint is called by the marketplace at all swap starts.
This is being done by one of these entrypoints, depending on the swap's type:
- `addToMarketplace`
- `makeCounterOffer`

The chosen token's id and the marketplace's address are being reassigned with the NFT, and updates that entry at the `ledger`, which maps `token_id -> owner`.

**swap finalisation:**
The NFT's contract's `transfer` entrypoint is called by the marketplace at all swap ends.
This is being done by one of these entrypoints, depending on the swap's type:
- `collect`
- `acceptOffer`
- `acceptCounterOffer`

The buyer's address and token's id are being reassigned the amount of editions bought (currently one), and updates that entry at the `ledger`, which maps `token_id -> owner`.

---

## **Operation Schematics:**

**notes:**
- for all following graphs, solid lines represent cross-contract operations and dotted lines represent actions not involving cross-contract operations.

### **Swap Initialisation:**

```mermaid
flowchart TD
  A(addToMarketplace)
  B(transfer)
  C(swaps)
  D[seller]
  E[marketplace]
  A--->|owner, token_id|B
  subgraph marketplace
  A-.->|create swap|C
  end
  subgraph Fa2
  subgraph ledger
  D-.->|token_id|E
  end
  B-.->ledger
  end
```

### **Swap Finalization:**

```mermaid
flowchart TD
A(collect)
C(transfer)
D(seller)
E(buyer)
F(swaps)
H(seller)
I(royalties_address)
J(treasury)
subgraph marketplace contract
A
F
end
subgraph Fa2 contract
C
subgraph ledger
D
E
end
end
A-->|token_id|C
A--->|royalties|I
A--->|management_fee|J
A-.->|clear swap_id|F
C-.->|token_id|ledger
D-.->|token_id|E
A--->|seller_value|H
```

```mermaid
  sequenceDiagram
    autonumber
    Alice->>marketplace: collect
    marketplace->>nft: transfer
    marketplace->>Bob: payment token transfer (seller_value)
    marketplace->>Treasury: payment token transfer (management_fee)
    marketplace->>Royalties_address: payment token transfer (royalties)
    loop transfer
      nft-->>nft: transfer NFT from Bob to Alice
    end
```

### **Accepting Offers:**

```mermaid
flowchart TD
A(acceptOffer)
B(royalties_address)
C(Treasury)
E(offers)
F(transfer)
G(buyer)
H(owner)
J(marketplace)
K(seller)
subgraph marketplace contract
A
E
end
subgraph Fa2 contract
    subgraph ledger
    G
    H
    J
    end
  F
end
A-->|"token_id, owner_tokens(OPTIONAL), swap_tokens"|F
F-.->|token_id|ledger
H-.->|NFT|G
J-.->|NFT|G
A-.->|clear offer_id|E
A--->|seller_value|K
A--->|royalties|B
A--->|management_fee|C
```
