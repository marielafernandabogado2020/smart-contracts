#if !REVEAL
#define REVEAL
#include "./reveal_interface.mligo"
#include "./reveal_constants.mligo"

[@inline]
let update_metadata_with_function_call (token_address : address) (param : update_metadata_with_function_param) : operation =
  match (Tezos.get_entrypoint_opt "%updateMetadataWithFunction" token_address : update_metadata_with_function_param contract option) with
  | None -> (failwith error_NFT_CONTRACT_MUST_HAVE_AN_UPDATE_METADATA_WITH_FUNCTION_ENTRYPOINT : operation)
  | Some update_metadata_with_function_entrypoint -> Tezos.transaction param 0mutez update_metadata_with_function_entrypoint

let set_pause (param : set_pause_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.factory then
    (failwith("only factory can call this entrypoint") : return)
  else
    ([] : operation list), { store with paused = param }

let set_nft_address (new_nft_address: set_nft_address_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.factory then
    (failwith("only factory can call this entrypoint reveal") : return)
  else
    ([] : operation list), { store with nft_address = new_nft_address }

let set_oracle_address (new_oracle_address: set_oracle_address_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
  ([] : operation list), { store with oracle = new_oracle_address }

let set_free_metadata (param : set_free_metadata_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
    let add_free_metadata (acc, el: free_metadata * (nat * metadata_id)) : free_metadata = Big_map.update el.0 (Some el.1) acc in
    let new_store =
      if param.version = 1n then
        let new_free_metadata = Map.fold add_free_metadata param.free_metadata store.free_metadata_v1 in
        { store with free_metadata_v1 = new_free_metadata; free_metadata_v1_length = param.free_metadata_length }
      else if param.version = 2n then
        let new_free_metadata = Map.fold add_free_metadata param.free_metadata store.free_metadata_v2 in
        { store with free_metadata_v2 = new_free_metadata; free_metadata_v2_length = param.free_metadata_length }
      else
        (failwith("version not found") : storage)
    in
    ([] : operation list), new_store

let set_token_id_to_metadata_id (param : set_token_id_to_metadata_id_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
    let add_token_id_to_metadata_id (acc, el: token_id_to_metadata_id * (token_id * metadata_id)) : token_id_to_metadata_id = Big_map.update el.0 (Some el.1) acc in
    let new_token_id_to_metadata_id = Map.fold add_token_id_to_metadata_id param store.token_id_to_metadata_id in
    ([] : operation list), { store with token_id_to_metadata_id = new_token_id_to_metadata_id }

let set_random_offset (param: set_random_offset_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
    let random =
      match (Tezos.call_view "getRandomBetween" param store.oracle :  nat option) with
      | None -> (failwith(error_ORACLE_FAILED_TO_SUPPLY_RANDOM_NUMBER) :  nat)
      | Some value -> value in
    ([] : operation list), { store with random_offset = random }

let assign_metadata_v1 (token_id: assign_metadata_param) (store: storage) : return =
  if store.free_metadata_v1_length < 1n then
    (failwith "No free metadata available" : return)
  else
    let random_number = store.random_offset + (Tezos.get_level ()) in
    let free_metadata_v1_index = (random_number mod store.free_metadata_v1_length) + 1n in
    let metadata_id =
      match Big_map.find_opt free_metadata_v1_index store.free_metadata_v1 with
      | Some index -> index
      | None -> (failwith "Index does not exist" : nat)
    in
    let new_token_id_to_metadata_id = Big_map.update token_id (Some metadata_id) store.token_id_to_metadata_id in
    let tail_metadata_id =
      match Big_map.find_opt store.free_metadata_v1_length store.free_metadata_v1 with
      | Some metadata_id -> metadata_id
      | None -> (failwith "Did not find tail metadata id" : nat)
    in
    let new_free_metadata_v1 = Big_map.update free_metadata_v1_index (Some tail_metadata_id) store.free_metadata_v1 in
    let new_free_metadata_v1 = Big_map.update store.free_metadata_v1_length (None : metadata_id option) new_free_metadata_v1 in
    let new_free_metadata_v1_length =
      match is_nat (store.free_metadata_v1_length - 1) with
      | Some n -> n
      | None -> 0n
    in
    let new_store = {
      store
      with
      random_offset = metadata_id;
      token_id_to_metadata_id = new_token_id_to_metadata_id;
      free_metadata_v1 = new_free_metadata_v1;
      free_metadata_v1_length = new_free_metadata_v1_length;
    }
    in
    ([] : operation list), new_store

let assign_metadata_v2 (token_id: assign_metadata_param) (store: storage) : return =
  if store.reveal_time > (Tezos.get_now ()) then
    (failwith error_REVEAL_PERIOD_IS_NOT_OPEN : return)
  else
    if store.free_metadata_v2_length < 1n then
      (failwith "No free metadata available" : return)
    else
      let random_number = store.random_offset + (Tezos.get_level ()) in
      let free_metadata_v2_index = (random_number mod store.free_metadata_v2_length) + 1n in
      let metadata_id =
        match Big_map.find_opt free_metadata_v2_index store.free_metadata_v2 with
        | Some index -> index
        | None -> (failwith "Index does not exist" : nat)
      in
      let new_token_id_to_metadata_id = Big_map.update token_id (Some metadata_id) store.token_id_to_metadata_id in
      let tail_metadata_id =
        match Big_map.find_opt store.free_metadata_v2_length store.free_metadata_v2 with
        | Some metadata_id -> metadata_id
        | None -> (failwith "Did not find tail metadata id" : nat)
      in
      let new_free_metadata_v2 = Big_map.update free_metadata_v2_index (Some tail_metadata_id) store.free_metadata_v2 in
      let new_free_metadata_v2 = Big_map.update store.free_metadata_v2_length (None : metadata_id option) new_free_metadata_v2 in
      let new_free_metadata_v2_length =
        match is_nat (store.free_metadata_v2_length - 1) with
        | Some n -> n
        | None -> 0n
      in
      let new_store = {
        store
        with
        random_offset = metadata_id;
        token_id_to_metadata_id = new_token_id_to_metadata_id;
        free_metadata_v2 = new_free_metadata_v2;
        free_metadata_v2_length = new_free_metadata_v2_length;
      }
      in
      ([] : operation list), new_store

let assign_metadata (token_id: assign_metadata_param) (store: storage) : return =
  if store.paused = true then
    (failwith error_CONTRACT_IN_PAUSE : return)
  else
    let balance_of_request = {
      token_id = token_id;
      owner = (Tezos.get_sender ());
    } in
    let _check_already_assigned =
      match Big_map.find_opt token_id store.token_id_to_metadata_id with
      | Some _ -> failwith "Token already has an assigned metadata"
      | None -> ()
    in
    match (Tezos.call_view "balance_of_view" balance_of_request store.nft_address : nat option) with
    | None -> (failwith "View returned an error" : return)
    | Some user_balance ->
      if user_balance <> 1n then
        (failwith "You do not own this token" : return)
      else
        if token_id <= 8000n then
          assign_metadata_v1 token_id store
        else if token_id <= 12000n then
          assign_metadata_v2 token_id store
        else
          (failwith("token_id out of range") : return)

let attributes_from_param (param : attributes_from_param_param) : (string, (string option * string)) map =
  Map.literal [
    ("Birthday", ((Some "date"), param.a));
    ("Bonding level", ((Some "number"), param.b));
    ("Breed", ((None : string option), param.c));
    ("Breeding count", ((Some "number"), param.d));
    ("Eyes color", ((None : string option), param.e));
    ("Friendliness", ((Some "number"), param.f));
    ("Fur color", ((None : string option), param.g));
    ("Gender", ((None : string option), param.h));
    ("Generation", ((None : string option), param.i));
    ("Group", ((None : string option), param.j));
    ("Intelligence", ((Some "number"), param.k));
    ("Obedience", ((Some "number"), param.l));
    ("Primary personality", ((None : string option), param.m));
    ("Rarity score", ((Some "number"), param.n));
    ("Rarity tier", ((None : string option), param.o));
    ("Secondary personality", ((None : string option), param.p));
    ("Size", ((None : string option), param.q));
    ("Status", ((None : string option), param.r));
    ("Strength", ((Some "number"), param.s));
    ("Vitality", ((Some "number"), param.t));
  ]

let add_reveal_admin (param: add_reveal_admin_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
    ([] : operation list), { store with reveal_admins = Set.add param store.reveal_admins }

let remove_reveal_admin (param: remove_reveal_admin_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
    ([] : operation list), { store with reveal_admins = Set.remove param store.reveal_admins }

let reveal (param : reveal_param) (store: storage) (operations : operation list) : return =
  if (not Set.mem (Tezos.get_sender ()) store.reveal_admins) then
    failwith error_UNAUTHORIZED_ADMIN_ADDRESS
  else
    let _metadata_id =
      match Big_map.find_opt param.token_id store.token_id_to_metadata_id with
      | None -> (failwith "You have not assigned a metadata yet" : nat)
      | Some metadata_id ->
        if metadata_id <> param.metadata.metadata_id then
          (failwith "Token id not matching metadata id" : nat)
        else
          metadata_id
    in
    let reveal_metadata_updater (token_metadata : token_metadata): token_metadata =
      let new_token_metadata = Map.update "formats" (Some param.metadata.formats) token_metadata in
      let new_token_metadata = Map.update "artifactUri" (Some param.metadata.artifactUri) new_token_metadata in
      let new_token_metadata = Map.update "displayUri" (Some param.metadata.displayUri) new_token_metadata in
      let new_token_metadata = Map.update "thumbnailUri" (Some param.metadata.thumbnailUri) new_token_metadata in
      Map.update "attributes" (Some (Bytes.pack (attributes_from_param(param.metadata.attributes)))) new_token_metadata
    in
    let reveal_param = {
      token_id = param.token_id;
      metadata_updater = reveal_metadata_updater;
    } in
    let reveal_metadata_op = update_metadata_with_function_call store.nft_address reveal_param in
    (reveal_metadata_op :: operations, store)

let set_ipfs_hashes (param : set_ipfs_hashes_param) (store: storage) : return =
  if (Tezos.get_sender ()) <> store.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
    ([] : operation list), { store with ipfs_hashes = param }

let main (action, store : parameter * storage) : return =
  match action with
    | SetPause p -> set_pause p store
    | SetNftAddress p -> set_nft_address p store
    | SetOracleAddress p -> set_oracle_address p store
    | SetFreeMetadata p -> set_free_metadata p store
    | SetTokenIdToMetadataId p -> set_token_id_to_metadata_id p store
    | SetRandomOffset p -> set_random_offset p store
    | AssignMetadata p -> assign_metadata p store
    | AddRevealAdmin p -> add_reveal_admin p store
    | RemoveRevealAdmin p -> remove_reveal_admin p store
    | Reveal p -> reveal p store ([] : operation list)
    | SetIpfsHashes p -> set_ipfs_hashes p store

#endif