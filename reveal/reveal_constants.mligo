#if !REVEAL_CONSTANTS
#define REVEAL_CONSTANTS

[@inline] let error_NFT_CONTRACT_MUST_HAVE_AN_UPDATE_METADATA_WITH_FUNCTION_ENTRYPOINT = "NFT contract must have an update metadata with function entrypoint"
[@inline] let error_REVEAL_PERIOD_IS_NOT_OPEN = "Reveal period is not open"
[@inline] let error_CONTRACT_IN_PAUSE = "Contract in pause"
[@inline] let error_ORACLE_FAILED_TO_SUPPLY_RANDOM_NUMBER = "Oracle failed to supply random number"
[@inline] let error_UNAUTHORIZED_ADMIN_ADDRESS = "Unauthorized admin address"

#endif