#if !REVEAL_INTERFACE
#define REVEAL_INTERFACE

#include "../nft/fa2_interface.mligo"

type entrypoint_signature =
[@layout:comb]
{
    name : string;
    params : bytes;
    source_contract : address;
}

type call_param =
[@layout:comb]
{
    entrypoint_signature : entrypoint_signature;
    callback : unit -> operation list;
}

(* storage types *)
type metadata_id = nat
type token_id_to_metadata_id = (token_id, metadata_id) big_map
type free_metadata = (nat, metadata_id) big_map
type storage =
[@layout:comb]
{
  paused: bool;
  controller : address;
  factory : address;
  nft_address: address;
  reveal_time: timestamp;
  free_metadata_v1: free_metadata;
  free_metadata_v1_length: nat;
  free_metadata_v2: free_metadata;
  free_metadata_v2_length: nat;
  token_id_to_metadata_id: token_id_to_metadata_id;
  random_offset: nat;
  reveal_admins: address set;
  oracle : address;
  ipfs_hashes: string;
}

(* param types *)
type set_pause_param = bool
type set_nft_address_param = address
type set_oracle_address_param = address
type update_Multisig_param = address
type set_free_metadata_param = {
  version: nat;
  free_metadata: (nat, metadata_id) map;
  free_metadata_length: nat;
}
type set_token_id_to_metadata_id_param = (token_id, metadata_id) map
type set_random_offset_param =
{
    from_: nat;
    to_: nat;
}
type assign_metadata_param = token_id
type add_reveal_admin_param = address
type remove_reveal_admin_param = address
type attributes_from_param_param =
{
  a: string;
  b: string;
  c: string;
  d: string;
  e: string;
  f: string;
  g: string;
  h: string;
  i: string;
  j: string;
  k: string;
  l: string;
  m: string;
  n: string;
  o: string;
  p: string;
  q: string;
  r: string;
  s: string;
  t: string;
}
type reveal_param_inner =
{
  artifactUri: bytes;
  attributes: attributes_from_param_param;
  displayUri: bytes;
  formats: bytes;
  thumbnailUri: bytes;
  metadata_id: nat;
}
type reveal_param =
{
  metadata: reveal_param_inner;
  token_id: token_id;
}
type set_ipfs_hashes_param = string

type return = operation list * storage

type parameter =
| SetPause of set_pause_param
| SetNftAddress of set_nft_address_param
| SetOracleAddress of set_oracle_address_param
| SetFreeMetadata of set_free_metadata_param
| SetTokenIdToMetadataId of set_token_id_to_metadata_id_param
| SetRandomOffset of set_random_offset_param
| AssignMetadata of assign_metadata_param
| AddRevealAdmin of add_reveal_admin_param
| RemoveRevealAdmin of remove_reveal_admin_param
| Reveal of reveal_param
| SetIpfsHashes of set_ipfs_hashes_param

#endif
