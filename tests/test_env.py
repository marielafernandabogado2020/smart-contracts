import unittest
import json
from dataclasses import dataclass, field
from dataclasses import field
from data import normalizer_data

from pytezos import pytezos
from pytezos import ContractInterface
from pytezos.contract.result import OperationResult


class Keys:
    def __init__(self, shell="http://localhost:8732"):
        self.ALICE_KEY = "edsk3EQB2zJvvGrMKzkUxhgERsy6qdDDw19TQyFWkYNUmGSxXiYm7Q"
        self.ALICE_PK = "tz1Yigc57GHQixFwDEVzj5N1znSCU3aq15td"
        self.BOB_KEY = "edsk4YDWx5QixxHtEfp5gKuYDd1AZLFqQhmquFgz64mDXghYYzW6T9"
        self.BOB_PK = "tz1RTrkJszz7MgNdeEvRLaek8CCrcvhTZTsg"
        self.RESERVE_KEY = "edsk3G87qnDZhR74qYDFAC6nE17XxWkvPJtWpLw4vfeZ3otEWwwskV"
        self.RESERVE_PK = "tz1iYCR11SMJcpAH3egtDjZRQgLgKX6agU7s"
        self.SHELL = shell

        self.alice_using_params = dict(shell=self.SHELL, key=self.ALICE_KEY)
        self.alice_pytezos = pytezos.using(**self.alice_using_params)

        self.bob_using_params = dict(shell=self.SHELL, key=self.BOB_KEY)
        self.bob_pytezos = pytezos.using(**self.bob_using_params)

        self.reserve_using_params = dict(
            shell=self.SHELL, key=self.RESERVE_KEY)
        self.reserve_pytezos = pytezos.using(**self.reserve_using_params)


default_keys = Keys()

# ORACLE_ADDRESS = "KT19etCHSt75MTF4NvUHxRNBPvp74ggv9g3P" # hangzhou
# ORACLE_ADDRESS = "KT1UcszCkuL5eMpErhWxpRmuniAecD227Dwp" # mainnet


def deploy_randomizer():
    '''used to get ORACLE_ADDRESS for tests'''
    bytes_to_nat = {}
    for i in range(256):
        k = i.to_bytes(1, 'little')
        bytes_to_nat[k] = i

    with open("../michelson/randomizer.tz", encoding="UTF-8") as mich_file:
        michelson = mich_file.read()

    randomizer = ContractInterface.from_michelson(
        michelson).using(**default_keys.alice_using_params)

    storage = {
        "admins": {ALICE_PK},
        "bytes_to_nat": bytes_to_nat,
        "entropy": "d75c55ae42c396569ac543c239e71d103fcb361ce2155e03349ca0c5cb830753",
        "metadata": {},
    }
    opg = randomizer.originate(initial_storage=storage).send(**send_conf)
    randomizer_addr = OperationResult.from_operation_group(opg.opg_result)[
        0
    ].originated_contracts[0]
    randomizer = pytezos.using(
        **default_keys.alice_using_params).contract(randomizer_addr)

    return randomizer


@ dataclass
class FA2Storage:
    multisig: str = default_keys.ALICE_PK
    contracts: set = field(default_factory=lambda: {default_keys.ALICE_PK})
    controller: str = default_keys.ALICE_PK
    factory: str = default_keys.ALICE_PK


@ dataclass
class MultisigStorage:
    authorized_contracts: str = default_keys.ALICE_PK
    admins: str = default_keys.ALICE_PK


@ dataclass
class AuctionStorage:
    multisig: str
    treasury: str = default_keys.RESERVE_PK
    nft_address: str = default_keys.ALICE_PK
    royalties_address: str = default_keys.ALICE_PK
    oracle: str = default_keys.ALICE_PK
    xtz_address: str = default_keys.ALICE_PK
    usd_address: str = default_keys.ALICE_PK
    eurl_address: str = default_keys.ALICE_PK
    oracle_tolerance: int = 900


@ dataclass
class MarketplaceStorage:
    multisig: str
    treasury: str = default_keys.RESERVE_PK
    nft_address: str = default_keys.ALICE_PK
    royalties_address: str = default_keys.ALICE_PK
    oracle: str = default_keys.ALICE_PK
    xtz_address: str = default_keys.ALICE_PK
    usd_address: str = default_keys.ALICE_PK
    doga_address: str = default_keys.ALICE_PK
    oracle_tolerance: int = 900


@ dataclass
class NormalizerStorage:
    oracleContract: str = "KT19yapgDLPR3CuvK32xJHgxNJigyNxjSr4y"


@dataclass
class FactoryStorage:
    multisig: str = default_keys.ALICE_PK
    nft_address: str = default_keys.ALICE_PK
    doga_address: str = default_keys.ALICE_PK
    reserve_address: str = default_keys.ALICE_PK
    oracle_address: str = default_keys.ALICE_PK


@ dataclass
class FA12Storage:
    admin: str = default_keys.ALICE_PK


ALICE_PK = default_keys.ALICE_PK
BOB_PK = default_keys.BOB_PK

alice_pytezos = default_keys.alice_pytezos
bob_pytezos = default_keys.bob_pytezos

send_conf = dict(min_confirmations=1)


class Env:
    def __init__(self, shell=default_keys.SHELL):
        self.alice_using_params = Keys(shell=shell).alice_using_params

    def deploy_multisig(self):
        with open("../michelson/multisig.tz", encoding="UTF-8") as mich_file:
            michelson = mich_file.read()

        multisig = ContractInterface.from_michelson(
            michelson).using(**self.alice_using_params)

        init_storage = MultisigStorage()

        storage = {
            "admins": {init_storage.admins},
            "n_calls": {},
            "threshold": 1,
            "duration": 3600,
            "authorized_contracts": {init_storage.authorized_contracts},
        }
        opg = multisig.originate(initial_storage=storage).send(**send_conf)
        multisig_addr = OperationResult.from_operation_group(opg.opg_result)[
            0
        ].originated_contracts[0]
        multisig = pytezos.using(
            **self.alice_using_params).contract(multisig_addr)

        return multisig

    def deploy_nft(self, init_storage: FA2Storage):
        with open("../michelson/nft.tz", "r", encoding="UTF8") as file:
            michelson = file.read()

        fa2 = ContractInterface.from_michelson(michelson).using(
            **self.alice_using_params
        )
        storage = {
            "contract_paused": False,
            "burn_paused": False,
            "total_supply": 0,
            "contracts": init_storage.contracts,
            "ledger": {},
            "operators": {},
            "metadata": {
                "": "tezos-storage:content".encode().hex(),
                "content": json.dumps({
                    "name": "DOGAMI",
                    "version": "1.0.0",
                    "homepage": "https://dogami.com/",
                    "authors": ["DOGAMI <hello@dogami.com>"],
                    "interfaces": ["TZIP-012", "TZIP-016", "TZIP-021"]
                }).encode().hex()
            },
            "token_metadata": {},
            "controller": init_storage.controller,
            "factory": init_storage.factory
        }
        opg = fa2.originate(initial_storage=storage).send(**send_conf)
        fa2_addr = OperationResult.from_operation_group(opg.opg_result)[
            0
        ].originated_contracts[0]
        fa2 = alice_pytezos.using(**self.alice_using_params).contract(fa2_addr)

        return fa2

    def deploy_fa12(self, init_storage: FA12Storage):
        with open("../michelson/fa12.tz", encoding="UTF-8") as mich_file:
            michelson = mich_file.read()
        fa12 = ContractInterface.from_michelson(
            michelson).using(**self.alice_using_params)
        storage = {
            "tokens": {},
            "allowances": {},
            "total_supply": 0,
            "metadata": {},
            "token_metadata": {},
            "paused": False,
            "admin": init_storage.admin
        }
        opg = fa12.originate(initial_storage=storage).send(**send_conf)
        fa12_addr = OperationResult.from_operation_group(opg.opg_result)[
            0
        ].originated_contracts[0]
        fa12 = pytezos.using(**self.alice_using_params).contract(fa12_addr)

        return fa12

    def deploy_normalizer(self):
        with open("../michelson/normalizer.tz", encoding="UTF-8") as mich_file:
            michelson = mich_file.read()
        normalizer = ContractInterface.from_michelson(michelson).using(
            **self.alice_using_params
        )
        storage = normalizer_data
        opg = normalizer.originate(initial_storage=storage).send(**send_conf)
        normalizer_addr = OperationResult.from_operation_group(opg.opg_result)[
            0
        ].originated_contracts[0]
        normalizer = pytezos.using(
            **self.alice_using_params).contract(normalizer_addr)

        return normalizer

    def deploy_marketplace(self, init_storage: MarketplaceStorage):

        with open("../michelson/marketplace.tz", encoding="UTF-8") as mich_file:
            michelson = mich_file.read()
        marketplace = ContractInterface.from_michelson(michelson).using(
            **self.alice_using_params
        )
        storage = {
            "multisig": init_storage.multisig,
            "treasury": init_storage.treasury,
            "collections": {init_storage.nft_address},
            "royalties_address": init_storage.royalties_address,
            "royalties_rate": 200,
            "next_swap_id": 0,
            "tokens": {},
            "swaps": {},
            "offers": {},
            "counter_offers": {},
            "management_fee_rate": 250,
            "paused": False,
            "allowed_tokens": {
                "XTZ": {
                    "token_symbol": "XTZ",
                    "fa_address": init_storage.xtz_address,
                    "fa_type": "XTZ"
                },
                "USD": {
                    "token_symbol": "USD",
                    "fa_address": init_storage.usd_address,
                    "fa_type": "fa1.2"
                },
                "DOGA": {
                    "token_symbol": "DOGA",
                    "fa_address": init_storage.doga_address,
                    "fa_type": "fa1.2"
                }
            },
            "available_pairs": {
                ("XTZ", "USD"): "XTZ-USD",
            },
            "oracle": init_storage.oracle,
            "oracle_tolerance": init_storage.oracle_tolerance,
        }
        opg = marketplace.originate(initial_storage=storage).send(**send_conf)
        marketplace_address = OperationResult.from_operation_group(opg.opg_result)[
            0
        ].originated_contracts[0]
        marketplace = pytezos.using(
            **self.alice_using_params).contract(marketplace_address)
        return marketplace

    def deploy_doga(self):
        with open("../michelson/doga.tz", "r", encoding="UTF8") as file:
            michelson = file.read()
        storage = {
            "paused": False,
            "burn_paused": False,
            "ledger": {ALICE_PK: 1_000_000_000},
            "allowances": {},
            "total_supply": 1_000_000_000,
            "metadata": {},
            "token_metadata": {},
            "multisig": ALICE_PK,
        }
        doga = ContractInterface.from_michelson(
            michelson).using(**self.alice_using_params)
        opg = doga.originate(initial_storage=storage).send(**send_conf)
        doga_addr = OperationResult.from_operation_group(
            opg.opg_result)[0].originated_contracts[0]
        doga = alice_pytezos.using(
            **self.alice_using_params).contract(doga_addr)
        return doga

    def deploy_mint_launcher(self, factory_address: str, controller_address: str):
        with open("../michelson/mint_launcher.tz") as mich_file:
            michelson = mich_file.read()
        mint_launcher = ContractInterface.from_michelson(
            michelson).using(**self.alice_using_params)
        storage = {
            "controller": controller_address,
            "factory": factory_address,
        }
        opg = mint_launcher.originate(
            initial_storage=storage).send(**send_conf)
        mint_launcher_address = OperationResult.from_operation_group(opg.opg_result)[
            0].originated_contracts[0]
        mint_launcher = pytezos.using(
            **self.alice_using_params).contract(mint_launcher_address)

        return mint_launcher

    def deploy_reveal_launcher(self, factory_address: str, controller_address: str):
        with open("../michelson/reveal_launcher.tz") as mich_file:
            michelson = mich_file.read()
        reveal_launcher = ContractInterface.from_michelson(
            michelson).using(**self.alice_using_params)
        storage = {
            "controller": controller_address,
            "factory": factory_address,
        }
        opg = reveal_launcher.originate(
            initial_storage=storage).send(**send_conf)
        reveal_launcher_address = OperationResult.from_operation_group(opg.opg_result)[
            0].originated_contracts[0]
        reveal_launcher = pytezos.using(
            **self.alice_using_params).contract(reveal_launcher_address)

        return reveal_launcher

    def deploy_nft_launcher(self, factory_address: str, controller_address: str):
        with open("../michelson/nft_launcher.tz") as mich_file:
            michelson = mich_file.read()
        nft_launcher = ContractInterface.from_michelson(
            michelson).using(**self.alice_using_params)
        storage = {
            "controller": controller_address,
            "factory": factory_address,
        }
        opg = nft_launcher.originate(
            initial_storage=storage).send(**send_conf)
        nft_launcher_address = OperationResult.from_operation_group(opg.opg_result)[
            0].originated_contracts[0]
        nft_launcher = pytezos.using(
            **self.alice_using_params).contract(nft_launcher_address)

        return nft_launcher

    def deploy_mint_controller(self, multisig: str, factory: str):
        with open("../michelson/mint_controller.tz") as mich_file:
            michelson = mich_file.read()
        mint_controller = ContractInterface.from_michelson(
            michelson).using(**self.alice_using_params)
        storage = {
            "multisig": multisig,
            "factory": factory,
        }
        opg = mint_controller.originate(
            initial_storage=storage).send(**send_conf)
        mint_controller_address = OperationResult.from_operation_group(opg.opg_result)[
            0].originated_contracts[0]
        mint_controller = pytezos.using(
            **self.alice_using_params).contract(mint_controller_address)

        return mint_controller

    def deploy_reveal_controller(self, multisig: str, factory: str):
        with open("../michelson/reveal_controller.tz") as mich_file:
            michelson = mich_file.read()
        reveal_controller = ContractInterface.from_michelson(
            michelson).using(**self.alice_using_params)
        storage = {
            "multisig": multisig,
            "factory": factory,
        }
        opg = reveal_controller.originate(
            initial_storage=storage).send(**send_conf)
        reveal_controller_address = OperationResult.from_operation_group(opg.opg_result)[
            0].originated_contracts[0]
        reveal_controller = pytezos.using(
            **self.alice_using_params).contract(reveal_controller_address)

        return reveal_controller

    def deploy_nft_controller(self, multisig: str, factory: str):
        with open("../michelson/nft_controller.tz") as mich_file:
            michelson = mich_file.read()
        nft_controller = ContractInterface.from_michelson(
            michelson).using(**self.alice_using_params)
        storage = {
            "multisig": multisig,
            "factory": factory,
        }
        opg = nft_controller.originate(
            initial_storage=storage).send(**send_conf)
        nft_controller_address = OperationResult.from_operation_group(opg.opg_result)[
            0].originated_contracts[0]
        nft_controller = pytezos.using(
            **self.alice_using_params).contract(nft_controller_address)

        return nft_controller

    def deploy_factory(self, multisig=ALICE_PK):
        init_storage = FactoryStorage(multisig=multisig)

        with open("../michelson/factory.tz", "r", encoding="UTF-8") as file:
            michelson = file.read()

        storage = {
            "last_deployed_contracts": {"minter": ALICE_PK, "revealer": ALICE_PK, "nft": ALICE_PK},
            "contract_sets": {},
            "multisig": init_storage.multisig,
            "doga_address": init_storage.doga_address,
            "default_reserve": init_storage.reserve_address,
            "default_oracle": init_storage.oracle_address,
            "mint_launcher": ALICE_PK,
            "reveal_launcher": ALICE_PK,
            "nft_launcher": ALICE_PK,
            "mint_controller": ALICE_PK,
            "reveal_controller": ALICE_PK,
            "nft_controller": ALICE_PK,
        }

        factory = ContractInterface.from_michelson(
            michelson).using(**self.alice_using_params)
        opg = factory.originate(initial_storage=storage).send(**send_conf)
        factory_addr = OperationResult.from_operation_group(
            opg.opg_result)[0].originated_contracts[0]
        factory = alice_pytezos.using(
            **self.alice_using_params).contract(factory_addr)

        return factory

    def deploy_factory_app(self):
        multisig = self.deploy_multisig()
        factory = self.deploy_factory(multisig.address)
        nft_controller = self.deploy_nft_controller(
            multisig=multisig.address, factory=factory.address)
        mint_controller = self.deploy_mint_controller(
            multisig=multisig.address, factory=factory.address)
        reveal_controller = self.deploy_reveal_controller(
            multisig=multisig.address, factory=factory.address)
        nft_launcher = self.deploy_nft_launcher(
            factory_address=factory.address, controller_address=nft_controller.address)
        mint_launcher = self.deploy_mint_launcher(
            factory_address=factory.address, controller_address=mint_controller.address)
        reveal_launcher = self.deploy_reveal_launcher(
            factory_address=factory.address, controller_address=reveal_controller.address)
        multisig.addAuthorizedContract(factory.address).send(**send_conf)
        multisig.addAuthorizedContract(
            nft_controller.address).send(**send_conf)
        multisig.addAuthorizedContract(
            mint_controller.address).send(**send_conf)
        multisig.addAuthorizedContract(
            reveal_controller.address).send(**send_conf)
        factory.updateLaunchers({"mint_launcher": mint_launcher.address,
                                "reveal_launcher": reveal_launcher.address, "nft_launcher": nft_launcher.address}).send(**send_conf)
        factory.updateControllers({"mint_controller": mint_controller.address,
                                  "reveal_controller": reveal_controller.address, "nft_controller": nft_controller.address}).send(**send_conf)

        return factory, multisig, mint_controller, reveal_controller, nft_controller

    def deploy_marketplace_app(self):
        factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
        ).deploy_factory_app()
        oracle = self.deploy_normalizer()
        doga = self.deploy_doga()
        marketplace_init_storage = MarketplaceStorage(
            multisig=multisig.address, doga_address=doga.address)
        marketplace_init_storage.oracle = oracle.address
        marketplace = self.deploy_marketplace(marketplace_init_storage)
        multisig.addAuthorizedContract(marketplace.address).send(**send_conf)

        return marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller


class TestEnv(unittest.TestCase):

    def test_deploy_marketplace_app(self):
        print("TestEnv: test_deploy_marketplace_app...")
        nft, crowdsale, marketplace, oracle, usd_fa12, multisig = Env(
        ).deploy_marketplace_app()
        self.assertEqual(usd_fa12.storage["admin"](), ALICE_PK)
        self.assertIn(marketplace.address, nft.storage["contracts"]())
        self.assertIn(ALICE_PK, multisig.storage["admins"]())
        self.assertIn(marketplace.address,
                      multisig.storage["authorized_contracts"]())
        self.assertIn(crowdsale.address, nft.storage["contracts"]())
        self.assertEqual(marketplace.storage["nft_address"](), nft.address)
        self.assertEqual(marketplace.storage["multisig"](), multisig.address)
        self.assertEqual(
            marketplace.storage["treasury"](), default_keys.RESERVE_PK)
        self.assertEqual(marketplace.storage["oracle"](), oracle.address)
        self.assertEqual(crowdsale.storage["nft_address"](), nft.address)


if __name__ == "__main__":
    unittest.main()
