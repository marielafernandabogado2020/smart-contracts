from test_env import Env, Keys, FA2Storage
import unittest
from pytezos.rpc.errors import MichelsonError
import logging

logging.basicConfig(level=logging.INFO)

ALICE_PK = Keys().ALICE_PK
BOB_PK = Keys().BOB_PK
RESERVE_PK = Keys().RESERVE_PK

alice_pytezos = Keys().alice_pytezos
bob_pytezos = Keys().bob_pytezos
reserve_pytezos = Keys().reserve_pytezos

send_conf = dict(min_confirmations=1)


class TestMintController(unittest.TestCase):
    @ staticmethod
    def print_title(instance):
        print("Test mint: " + instance.__class__.__name__ + "...")
        print("-----------------------------------")

    @ staticmethod
    def print_success(function_name):
        print(function_name + "... ok")
        print("-----------------------------------")

    class SetMintParams(unittest.TestCase):
        def test00_before_tests(self):
            TestMintController.print_title(self)

        def test01_it_sets_mint_params_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            start_time = alice_pytezos.now() - 15
            end_time = alice_pytezos.now() + 100
            set_mint_params = {
                "set_mint_params": {
                    "is_private_sale": False,
                    "mint_price": 1,
                    "currency": "XTZ",
                    "next_token_id": 1,
                    "total_supply": 100,
                    "max_mint_per_address": 100,
                    "token_per_pack": 3,
                    "start_time": start_time,
                    "end_time": end_time,
                },
                "set_id": "set1",
            }
            mint_controller.setMintParams(set_mint_params).send(**send_conf)
            self.assertEqual(minter.storage["is_private_sale"](), False)
            self.assertEqual(minter.storage["mint_price"](), 1)
            self.assertEqual(minter.storage["currency"](), "XTZ")
            self.assertEqual(minter.storage["next_token_id"](), 1)
            self.assertEqual(minter.storage["total_supply"](), 100)
            self.assertEqual(minter.storage["max_mint_per_address"](), 100)
            self.assertEqual(minter.storage["token_per_pack"](), 3)
            self.assertEqual(minter.storage["start_time"](), start_time)
            self.assertEqual(minter.storage["end_time"](), end_time)
            TestMintController.print_success(
                "test01_it_sets_mint_params_successfully_one_set_with_self")

        def test02_it_sets_mint_params_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            start_time = alice_pytezos.now() - 15
            end_time = alice_pytezos.now() + 100
            set_mint_params = {
                "set_mint_params": {
                    "is_private_sale": False,
                    "mint_price": 1,
                    "currency": "XTZ",
                    "next_token_id": 1,
                    "total_supply": 100,
                    "max_mint_per_address": 100,
                    "token_per_pack": 3,
                    "start_time": start_time,
                    "end_time": end_time,
                },
                "set_id": "set1",
            }
            mint_controller.setMintParams(set_mint_params).send(**send_conf)
            bob_pytezos.contract(mint_controller.address).setMintParams(
                set_mint_params).send(**send_conf)
            self.assertEqual(minter.storage["is_private_sale"](), False)
            self.assertEqual(minter.storage["mint_price"](), 1)
            self.assertEqual(minter.storage["currency"](), "XTZ")
            self.assertEqual(minter.storage["next_token_id"](), 1)
            self.assertEqual(minter.storage["total_supply"](), 100)
            self.assertEqual(minter.storage["max_mint_per_address"](), 100)
            self.assertEqual(minter.storage["token_per_pack"](), 3)
            self.assertEqual(minter.storage["start_time"](), start_time)
            self.assertEqual(minter.storage["end_time"](), end_time)
            TestMintController.print_success(
                "test02_it_sets_mint_params_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            multisig.removeAuthorizedContract(
                mint_controller.address).send(**send_conf)
            start_time = alice_pytezos.now() - 15
            end_time = alice_pytezos.now() + 100
            set_mint_params = {
                "set_mint_params": {
                    "is_private_sale": False,
                    "mint_price": 1,
                    "currency": "XTZ",
                    "next_token_id": 1,
                    "total_supply": 100,
                    "max_mint_per_address": 100,
                    "token_per_pack": 3,
                    "start_time": start_time,
                    "end_time": end_time,
                },
                "set_id": "set1",
            }
            with self.assertRaises(MichelsonError) as err:
                mint_controller.setMintParams(
                    set_mint_params).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMintController.print_success(
                "test03_it_fails_when_factory_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            start_time = alice_pytezos.now() - 15
            end_time = alice_pytezos.now() + 100
            set_mint_params = {
                "set_mint_params": {
                    "is_private_sale": False,
                    "mint_price": 1,
                    "currency": "XTZ",
                    "next_token_id": 1,
                    "total_supply": 100,
                    "max_mint_per_address": 100,
                    "token_per_pack": 3,
                    "start_time": start_time,
                    "end_time": end_time,
                },
                "set_id": "set1",
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(mint_controller.address).setMintParams(
                    set_mint_params).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestMintController.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            start_time = alice_pytezos.now() - 15
            end_time = alice_pytezos.now() + 100
            set_mint_params = {
                "set_mint_params": {
                    "is_private_sale": False,
                    "mint_price": 1,
                    "currency": "XTZ",
                    "next_token_id": 1,
                    "total_supply": 100,
                    "max_mint_per_address": 100,
                    "token_per_pack": 3,
                    "start_time": start_time,
                    "end_time": end_time,
                },
                "set_id": "set1",
            }
            mint_controller.setMintParams(set_mint_params).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                mint_controller.setMintParams(
                    set_mint_params).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestMintController.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class SetMintPerAddress(unittest.TestCase):
        def test00_before_tests(self):
            TestMintController.print_title(self)

        def test01_it_sets_mint_per_address_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            start_time = alice_pytezos.now() - 15
            end_time = alice_pytezos.now() + 100
            mint_params = {
                "set_mint_params": {
                    "is_private_sale": False,
                    "mint_price": 1,
                    "currency": "XTZ",
                    "next_token_id": 1,
                    "total_supply": 100,
                    "max_mint_per_address": 100,
                    "token_per_pack": 3,
                    "start_time": start_time,
                    "end_time": end_time,
                },
                "set_id": "set1",
            }
            mint_controller.setMintParams(mint_params).send(**send_conf)

            price = mint_params["set_mint_params"]["mint_price"]
            minter.mintFromCrowdsale(
                {"token_count": 3, "to_": ALICE_PK}).with_amount(price * 3).send(**send_conf)

            self.assertEqual(
                minter.storage["mint_per_address"][ALICE_PK](), 3)

            mint_controller.setMintPerAddress("set1").send(**send_conf)
            with self.assertRaises(KeyError):
                minter.storage["mint_per_address"][ALICE_PK]()

            TestMintController.print_success(
                "test01_it_sets_mint_per_address_successfully")

        def test02_it_sets_mint_per_address_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            start_time = alice_pytezos.now() - 15
            end_time = alice_pytezos.now() + 100
            mint_params = {
                "set_mint_params": {
                    "is_private_sale": False,
                    "mint_price": 1,
                    "currency": "XTZ",
                    "next_token_id": 1,
                    "total_supply": 100,
                    "max_mint_per_address": 100,
                    "token_per_pack": 3,
                    "start_time": start_time,
                    "end_time": end_time,
                },
                "set_id": "set1",
            }
            mint_controller.setMintParams(mint_params).send(**send_conf)

            price = mint_params["set_mint_params"]["mint_price"]
            minter.mintFromCrowdsale(
                {"token_count": 3, "to_": ALICE_PK}).with_amount(price * 3).send(**send_conf)
            self.assertEqual(
                minter.storage["mint_per_address"][ALICE_PK](), 3)

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            mint_controller.setMintPerAddress("set1").send(**send_conf)
            self.assertEqual(
                minter.storage["mint_per_address"][ALICE_PK](), 3)
            bob_pytezos.contract(mint_controller.address).setMintPerAddress(
                "set1").send(**send_conf)
            with self.assertRaises(KeyError):
                minter.storage["mint_per_address"][ALICE_PK]()
            TestMintController.print_success(
                "test02_it_sets_mint_per_address_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            start_time = alice_pytezos.now() - 15
            end_time = alice_pytezos.now() + 100
            mint_params = {
                "set_mint_params": {
                    "is_private_sale": False,
                    "mint_price": 1,
                    "currency": "XTZ",
                    "next_token_id": 1,
                    "total_supply": 100,
                    "max_mint_per_address": 100,
                    "token_per_pack": 3,
                    "start_time": start_time,
                    "end_time": end_time,
                },
                "set_id": "set1",
            }
            mint_controller.setMintParams(mint_params).send(**send_conf)

            price = mint_params["set_mint_params"]["mint_price"]
            minter.mintFromCrowdsale(
                {"token_count": 3, "to_": ALICE_PK}).with_amount(price * 3).send(**send_conf)
            self.assertEqual(
                minter.storage["mint_per_address"][ALICE_PK](), 3)

            multisig.removeAuthorizedContract(
                mint_controller.address).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                mint_controller.setMintPerAddress("set1").send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMintController.print_success(
                "test03_it_fails_when_mint_controller_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            start_time = alice_pytezos.now() - 15
            end_time = alice_pytezos.now() + 100
            mint_params = {
                "set_mint_params": {
                    "is_private_sale": False,
                    "mint_price": 1,
                    "currency": "XTZ",
                    "next_token_id": 1,
                    "total_supply": 100,
                    "max_mint_per_address": 100,
                    "token_per_pack": 3,
                    "start_time": start_time,
                    "end_time": end_time,
                },
                "set_id": "set1",
            }
            mint_controller.setMintParams(mint_params).send(**send_conf)

            price = mint_params["set_mint_params"]["mint_price"]
            minter.mintFromCrowdsale(
                {"token_count": 3, "to_": ALICE_PK}).with_amount(price * 3).send(**send_conf)
            self.assertEqual(
                minter.storage["mint_per_address"][ALICE_PK](), 3)
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(mint_controller.address).setMintPerAddress(
                    "set1").send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestMintController.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            start_time = alice_pytezos.now() - 15
            end_time = alice_pytezos.now() + 100
            mint_params = {
                "set_mint_params": {
                    "is_private_sale": False,
                    "mint_price": 1,
                    "currency": "XTZ",
                    "next_token_id": 1,
                    "total_supply": 100,
                    "max_mint_per_address": 100,
                    "token_per_pack": 3,
                    "start_time": start_time,
                    "end_time": end_time,
                },
                "set_id": "set1",
            }
            mint_controller.setMintParams(mint_params).send(**send_conf)

            price = mint_params["set_mint_params"]["mint_price"]
            minter.mintFromCrowdsale(
                {"token_count": 3, "to_": ALICE_PK}).with_amount(price * 3).send(**send_conf)
            self.assertEqual(
                minter.storage["mint_per_address"][ALICE_PK](), 3)
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            mint_controller.setMintPerAddress("set1").send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                mint_controller.setMintPerAddress("set1").send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestMintController.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class AddToWhitelist(unittest.TestCase):
        def test00_before_tests(self):
            TestMintController.print_title(self)

        def test01_it_adds_to_whitelist_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            address_list = [(BOB_PK, False), (ALICE_PK, False),
                            (minter.address, False)]
            add_to_whitelist_param = {
                "add_to_whitelist_param": address_list,
                "set_id": "set1",
            }
            with self.assertRaises(KeyError):
                minter.storage["whitelist"][BOB_PK]()
            with self.assertRaises(KeyError):
                minter.storage["whitelist"][ALICE_PK]()
            with self.assertRaises(KeyError):
                minter.storage["whitelist"][minter.address]()

            mint_controller.addToWhitelist(
                add_to_whitelist_param).send(**send_conf)

            self.assertEqual(minter.storage["whitelist"][BOB_PK](), False)
            self.assertEqual(minter.storage["whitelist"][ALICE_PK](), False)
            self.assertEqual(
                minter.storage["whitelist"][minter.address](), False)

            TestMintController.print_success(
                "test01_it_adds_to_whitelist_successfully")

        def test02_it_adds_to_whitelist_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            address_list = [(BOB_PK, False), (ALICE_PK, False),
                            (minter.address, False)]
            add_to_whitelist_param = {
                "add_to_whitelist_param": address_list,
                "set_id": "set1",
            }
            with self.assertRaises(KeyError):
                minter.storage["whitelist"][BOB_PK]()
            with self.assertRaises(KeyError):
                minter.storage["whitelist"][ALICE_PK]()
            with self.assertRaises(KeyError):
                minter.storage["whitelist"][minter.address]()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            mint_controller.addToWhitelist(
                add_to_whitelist_param).send(**send_conf)
            with self.assertRaises(KeyError):
                minter.storage["whitelist"][BOB_PK]()
            with self.assertRaises(KeyError):
                minter.storage["whitelist"][ALICE_PK]()
            with self.assertRaises(KeyError):
                minter.storage["whitelist"][minter.address]()
            bob_pytezos.contract(mint_controller.address).addToWhitelist(
                add_to_whitelist_param).send(**send_conf)
            self.assertEqual(minter.storage["whitelist"][BOB_PK](), False)
            self.assertEqual(minter.storage["whitelist"][ALICE_PK](), False)
            self.assertEqual(
                minter.storage["whitelist"][minter.address](), False)
            TestMintController.print_success(
                "test02_it_adds_to_whitelist_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            address_list = [(BOB_PK, False), (ALICE_PK, False),
                            (minter.address, False)]
            add_to_whitelist_param = {
                "add_to_whitelist_param": address_list,
                "set_id": "set1",
            }
            multisig.removeAuthorizedContract(
                mint_controller.address).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                mint_controller.addToWhitelist(
                    add_to_whitelist_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMintController.print_success(
                "test03_it_fails_when_mint_controller_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            address_list = [(BOB_PK, False), (ALICE_PK, False),
                            (minter.address, False)]
            add_to_whitelist_param = {
                "add_to_whitelist_param": address_list,
                "set_id": "set1",
            }

            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(mint_controller.address).addToWhitelist(
                    add_to_whitelist_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestMintController.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            address_list = [(BOB_PK, False), (ALICE_PK, False),
                            (minter.address, False)]
            add_to_whitelist_param = {
                "add_to_whitelist_param": address_list,
                "set_id": "set1",
            }

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            mint_controller.addToWhitelist(
                add_to_whitelist_param).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                mint_controller.addToWhitelist(
                    add_to_whitelist_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestMintController.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class RemoveFromWhitelist(unittest.TestCase):
        def test00_before_tests(self):
            TestMintController.print_title(self)

        def test01_it_removes_from_whitelist_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            address_list = [(BOB_PK, False), (ALICE_PK, False),
                            (minter.address, False)]
            add_to_whitelist_param = {
                "add_to_whitelist_param": address_list,
                "set_id": "set1",
            }

            mint_controller.addToWhitelist(
                add_to_whitelist_param).send(**send_conf)

            self.assertEqual(minter.storage["whitelist"][BOB_PK](), False)
            self.assertEqual(minter.storage["whitelist"][ALICE_PK](), False)
            self.assertEqual(
                minter.storage["whitelist"][minter.address](), False)

            remove_from_whitelist_param = {
                "remove_from_whitelist_param": [BOB_PK, ALICE_PK],
                "set_id": "set1",
            }

            mint_controller.removeFromWhitelist(
                remove_from_whitelist_param).send(**send_conf)

            self.assertEqual(
                minter.storage["whitelist"][minter.address](), False)
            with self.assertRaises(KeyError):
                minter.storage["whitelist"][ALICE_PK]()
            with self.assertRaises(KeyError):
                minter.storage["whitelist"][BOB_PK]()

            TestMintController.print_success(
                "test01_it_removes_from_whitelist_successfully")

        def test02_it_removes_from_whitelist_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            address_list = [(BOB_PK, False), (ALICE_PK, False),
                            (minter.address, False)]
            add_to_whitelist_param = {
                "add_to_whitelist_param": address_list,
                "set_id": "set1",
            }
            mint_controller.addToWhitelist(
                add_to_whitelist_param).send(**send_conf)
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            remove_from_whitelist_param = {
                "remove_from_whitelist_param": [BOB_PK, ALICE_PK],
                "set_id": "set1",
            }

            mint_controller.removeFromWhitelist(
                remove_from_whitelist_param).send(**send_conf)
            self.assertEqual(minter.storage["whitelist"][BOB_PK](), False)
            self.assertEqual(minter.storage["whitelist"][ALICE_PK](), False)
            self.assertEqual(
                minter.storage["whitelist"][minter.address](), False)
            bob_pytezos.contract(mint_controller.address).removeFromWhitelist(
                remove_from_whitelist_param).send(**send_conf)
            self.assertEqual(
                minter.storage["whitelist"][minter.address](), False)
            with self.assertRaises(KeyError):
                minter.storage["whitelist"][ALICE_PK]()
            with self.assertRaises(KeyError):
                minter.storage["whitelist"][BOB_PK]()
            TestMintController.print_success(
                "test02_it_removes_from_whitelist_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            address_list = [(BOB_PK, False), (ALICE_PK, False),
                            (minter.address, False)]
            add_to_whitelist_param = {
                "add_to_whitelist_param": address_list,
                "set_id": "set1",
            }
            mint_controller.addToWhitelist(
                add_to_whitelist_param).send(**send_conf)
            multisig.removeAuthorizedContract(
                mint_controller.address).send(**send_conf)

            remove_from_whitelist_param = {
                "remove_from_whitelist_param": [BOB_PK, ALICE_PK],
                "set_id": "set1",
            }

            with self.assertRaises(MichelsonError) as err:
                mint_controller.removeFromWhitelist(
                    remove_from_whitelist_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMintController.print_success(
                "test03_it_fails_when_mint_controller_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            address_list = [(BOB_PK, False), (ALICE_PK, False),
                            (minter.address, False)]
            add_to_whitelist_param = {
                "add_to_whitelist_param": address_list,
                "set_id": "set1",
            }
            mint_controller.addToWhitelist(
                add_to_whitelist_param).send(**send_conf)

            remove_from_whitelist_param = {
                "remove_from_whitelist_param": [BOB_PK, ALICE_PK],
                "set_id": "set1",
            }

            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(mint_controller.address).removeFromWhitelist(
                    remove_from_whitelist_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestMintController.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            address_list = [(BOB_PK, False), (ALICE_PK, False),
                            (minter.address, False)]
            add_to_whitelist_param = {
                "add_to_whitelist_param": address_list,
                "set_id": "set1",
            }
            mint_controller.addToWhitelist(
                add_to_whitelist_param).send(**send_conf)

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            remove_from_whitelist_param = {
                "remove_from_whitelist_param": [BOB_PK, ALICE_PK],
                "set_id": "set1",
            }
            mint_controller.removeFromWhitelist(
                remove_from_whitelist_param).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                mint_controller.removeFromWhitelist(
                    remove_from_whitelist_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestMintController.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class BurnFromCrowdsale(unittest.TestCase):
        def test00_before_tests(self):
            TestMintController.print_title(self)

        def test01_it_burns_from_crowdsale_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)

            start_time = alice_pytezos.now() - 15
            end_time = alice_pytezos.now() + 100
            mint_params = {
                "set_mint_params": {
                    "is_private_sale": False,
                    "mint_price": 1,
                    "currency": "XTZ",
                    "next_token_id": 1,
                    "total_supply": 100,
                    "max_mint_per_address": 100,
                    "token_per_pack": 3,
                    "start_time": start_time,
                    "end_time": end_time,
                },
                "set_id": "set1",
            }
            mint_controller.setMintParams(mint_params).send(**send_conf)

            price = mint_params["set_mint_params"]["mint_price"]
            minter.mintFromCrowdsale(
                {"token_count": 3, "to_": ALICE_PK}).with_amount(price * 3).send(**send_conf)

            self.assertEqual(
                minter.storage["mint_per_address"][ALICE_PK](), 3)

            burn_params = {
                "set_id": "set1",
                "burn_from_crowdsale_param": {
                    "token_id": 1,
                    "from_": ALICE_PK
                }
            }
            nft.update_operators([{
                "add_operator": {
                    "owner": ALICE_PK,
                    "operator": minter.address,
                    "token_id": 1
                }}]).send(**send_conf)

            mint_controller.burnFromCrowdsale(burn_params).send(**send_conf)
            with self.assertRaises(KeyError):
                nft.storage["ledger"][1]()

            TestMintController.print_success(
                "test01_it_burns_from_crowdsale_successfully")

        def test02_it_burns_from_crowdsale_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)

            start_time = alice_pytezos.now() - 15
            end_time = alice_pytezos.now() + 100
            mint_params = {
                "set_mint_params": {
                    "is_private_sale": False,
                    "mint_price": 1,
                    "currency": "XTZ",
                    "next_token_id": 1,
                    "total_supply": 100,
                    "max_mint_per_address": 100,
                    "token_per_pack": 3,
                    "start_time": start_time,
                    "end_time": end_time,
                },
                "set_id": "set1",
            }
            mint_controller.setMintParams(mint_params).send(**send_conf)

            price = mint_params["set_mint_params"]["mint_price"]
            minter.mintFromCrowdsale(
                {"token_count": 3, "to_": ALICE_PK}).with_amount(price * 3).send(**send_conf)
            self.assertEqual(
                minter.storage["mint_per_address"][ALICE_PK](), 3)
            burn_params = {
                "set_id": "set1",
                "burn_from_crowdsale_param": {
                    "token_id": 1,
                    "from_": ALICE_PK
                }
            }
            nft.update_operators([{
                "add_operator": {
                    "owner": ALICE_PK,
                    "operator": minter.address,
                    "token_id": 1
                }}]).send(**send_conf)

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            mint_controller.burnFromCrowdsale(burn_params).send(**send_conf)
            self.assertEqual(
                nft.storage["ledger"][1](), ALICE_PK)
            bob_pytezos.contract(mint_controller.address).burnFromCrowdsale(
                burn_params).send(**send_conf)
            with self.assertRaises(KeyError):
                nft.storage["ledger"][1]()
            TestMintController.print_success(
                "test02_it_burns_from_crowdsale_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)

            start_time = alice_pytezos.now() - 15
            end_time = alice_pytezos.now() + 100
            mint_params = {
                "set_mint_params": {
                    "is_private_sale": False,
                    "mint_price": 1,
                    "currency": "XTZ",
                    "next_token_id": 1,
                    "total_supply": 100,
                    "max_mint_per_address": 100,
                    "token_per_pack": 3,
                    "start_time": start_time,
                    "end_time": end_time,
                },
                "set_id": "set1",
            }
            mint_controller.setMintParams(mint_params).send(**send_conf)

            price = mint_params["set_mint_params"]["mint_price"]
            minter.mintFromCrowdsale(
                {"token_count": 3, "to_": ALICE_PK}).with_amount(price * 3).send(**send_conf)
            self.assertEqual(
                minter.storage["mint_per_address"][ALICE_PK](), 3)
            burn_params = {
                "set_id": "set1",
                "burn_from_crowdsale_param": {
                    "token_id": 1,
                    "from_": ALICE_PK
                }
            }
            nft.update_operators([{
                "add_operator": {
                    "owner": ALICE_PK,
                    "operator": minter.address,
                    "token_id": 1
                }}]).send(**send_conf)

            multisig.removeAuthorizedContract(
                mint_controller.address).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                mint_controller.burnFromCrowdsale(
                    burn_params).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMintController.print_success(
                "test03_it_fails_when_mint_controller_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)

            start_time = alice_pytezos.now() - 15
            end_time = alice_pytezos.now() + 100
            mint_params = {
                "set_mint_params": {
                    "is_private_sale": False,
                    "mint_price": 1,
                    "currency": "XTZ",
                    "next_token_id": 1,
                    "total_supply": 100,
                    "max_mint_per_address": 100,
                    "token_per_pack": 3,
                    "start_time": start_time,
                    "end_time": end_time,
                },
                "set_id": "set1",
            }
            mint_controller.setMintParams(mint_params).send(**send_conf)

            price = mint_params["set_mint_params"]["mint_price"]
            minter.mintFromCrowdsale(
                {"token_count": 3, "to_": ALICE_PK}).with_amount(price * 3).send(**send_conf)
            self.assertEqual(
                minter.storage["mint_per_address"][ALICE_PK](), 3)
            burn_params = {
                "set_id": "set1",
                "burn_from_crowdsale_param": {
                    "token_id": 1,
                    "from_": ALICE_PK
                }
            }
            nft.update_operators([{
                "add_operator": {
                    "owner": ALICE_PK,
                    "operator": minter.address,
                    "token_id": 1
                }}]).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(mint_controller.address).burnFromCrowdsale(
                    burn_params).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestMintController.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)

            start_time = alice_pytezos.now() - 15
            end_time = alice_pytezos.now() + 100
            mint_params = {
                "set_mint_params": {
                    "is_private_sale": False,
                    "mint_price": 1,
                    "currency": "XTZ",
                    "next_token_id": 1,
                    "total_supply": 100,
                    "max_mint_per_address": 100,
                    "token_per_pack": 3,
                    "start_time": start_time,
                    "end_time": end_time,
                },
                "set_id": "set1",
            }
            mint_controller.setMintParams(mint_params).send(**send_conf)

            price = mint_params["set_mint_params"]["mint_price"]
            minter.mintFromCrowdsale(
                {"token_count": 3, "to_": ALICE_PK}).with_amount(price * 3).send(**send_conf)
            burn_params = {
                "set_id": "set1",
                "burn_from_crowdsale_param": {
                    "token_id": 1,
                    "from_": ALICE_PK
                }
            }
            nft.update_operators([{
                "add_operator": {
                    "owner": ALICE_PK,
                    "operator": minter.address,
                    "token_id": 1
                }}]).send(**send_conf)
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            mint_controller.burnFromCrowdsale(burn_params).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                mint_controller.burnFromCrowdsale(
                    burn_params).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestMintController.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class SetNameList(unittest.TestCase):
        def test00_before_tests(self):
            TestMintController.print_title(self)

        def test01_it_sets_name_list_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            name_list = {0: b"a", 1: b"b", 2: b"c"}
            set_name_list_param = {
                "set_id": "set1",
                "set_name_list_param": name_list
            }

            mint_controller.setNameList(set_name_list_param).send(**send_conf)
            self.assertEqual(minter.storage["name_list"][0](), b"a")
            self.assertEqual(minter.storage["name_list"][1](), b"b")
            self.assertEqual(minter.storage["name_list"][2](), b"c")

            TestMintController.print_success(
                "test01_it_sets_name_list_successfully")

        def test02_it_sets_name_list_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            name_list = {0: b"a", 1: b"b", 2: b"c"}
            set_name_list_param = {
                "set_id": "set1",
                "set_name_list_param": name_list
            }
            mint_controller.setNameList(set_name_list_param).send(**send_conf)
            with self.assertRaises(KeyError):
                minter.storage["name_list"][0]()
            with self.assertRaises(KeyError):
                minter.storage["name_list"][1]()
            with self.assertRaises(KeyError):
                minter.storage["name_list"][2]()
            bob_pytezos.contract(mint_controller.address).setNameList(
                set_name_list_param).send(**send_conf)
            self.assertEqual(minter.storage["name_list"][0](), b"a")
            self.assertEqual(minter.storage["name_list"][1](), b"b")
            self.assertEqual(minter.storage["name_list"][2](), b"c")
            TestMintController.print_success(
                "test02_it_sets_name_list_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            name_list = {0: b"a", 1: b"b", 2: b"c"}
            set_name_list_param = {
                "set_id": "set1",
                "set_name_list_param": name_list
            }

            multisig.removeAuthorizedContract(
                mint_controller.address).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                mint_controller.setNameList(
                    set_name_list_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMintController.print_success(
                "test03_it_fails_when_mint_controller_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            name_list = {0: b"a", 1: b"b", 2: b"c"}
            set_name_list_param = {
                "set_id": "set1",
                "set_name_list_param": name_list
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(mint_controller.address).setNameList(
                    set_name_list_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestMintController.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            name_list = {0: b"a", 1: b"b", 2: b"c"}
            set_name_list_param = {
                "set_id": "set1",
                "set_name_list_param": name_list
            }
            mint_controller.setNameList(set_name_list_param).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                mint_controller.setNameList(
                    set_name_list_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestMintController.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class SetDogaAddress(unittest.TestCase):
        def test00_before_tests(self):
            TestMintController.print_title(self)

        def test01_it_sets_doga_address_successfully_single(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            set_dog_address_param = {
                "update_method": {"single": "set1"},
                "doga_address": BOB_PK,
            }
            self.assertNotEqual(minter.storage["doga_address"](), BOB_PK)
            mint_controller.setDogaAddress(
                set_dog_address_param).send(**send_conf)
            self.assertEqual(minter.storage["doga_address"](), BOB_PK)

            TestMintController.print_success(
                "test01_it_sets_doga_address_successfully_single")

        def test02_it_sets_doga_address_successfully_multiple(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            minter2_addr = factory.storage["contract_sets"]["set2"]["minter"]()
            minter2 = alice_pytezos.contract(minter2_addr)
            set_dog_address_param = {
                "update_method": {"multiple": ["set1", "set2"]},
                "doga_address": BOB_PK,
            }
            self.assertNotEqual(minter.storage["doga_address"](), BOB_PK)
            self.assertNotEqual(minter2.storage["doga_address"](), BOB_PK)
            mint_controller.setDogaAddress(
                set_dog_address_param).send(**send_conf)
            self.assertEqual(minter.storage["doga_address"](), BOB_PK)
            self.assertEqual(minter2.storage["doga_address"](), BOB_PK)

            TestMintController.print_success(
                "test02_it_sets_doga_address_successfully_multiple")

        def test03_it_burns_from_crowdsale_with_two_admins_single(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            set_dog_address_param = {
                "update_method": {"single": "set1"},
                "doga_address": BOB_PK,
            }
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            self.assertNotEqual(minter.storage["doga_address"](), BOB_PK)
            mint_controller.setDogaAddress(
                set_dog_address_param).send(**send_conf)
            self.assertNotEqual(minter.storage["doga_address"](), BOB_PK)
            bob_pytezos.contract(mint_controller.address).setDogaAddress(
                set_dog_address_param).send(**send_conf)
            self.assertEqual(minter.storage["doga_address"](), BOB_PK)

            TestMintController.print_success(
                "test03_it_burns_from_crowdsale_with_two_admins_single")

        def test04_it_burns_from_crowdsale_with_two_admins_multiple(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            minter2_addr = factory.storage["contract_sets"]["set2"]["minter"]()
            minter2 = alice_pytezos.contract(minter2_addr)
            set_dog_address_param = {
                "update_method": {"multiple": ["set1", "set2"]},
                "doga_address": BOB_PK,
            }
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            self.assertNotEqual(minter.storage["doga_address"](), BOB_PK)
            self.assertNotEqual(minter2.storage["doga_address"](), BOB_PK)
            mint_controller.setDogaAddress(
                set_dog_address_param).send(**send_conf)
            self.assertNotEqual(minter.storage["doga_address"](), BOB_PK)
            self.assertNotEqual(minter2.storage["doga_address"](), BOB_PK)
            bob_pytezos.contract(mint_controller.address).setDogaAddress(
                set_dog_address_param).send(**send_conf)
            self.assertEqual(minter.storage["doga_address"](), BOB_PK)
            self.assertEqual(minter2.storage["doga_address"](), BOB_PK)

            TestMintController.print_success(
                "test04_it_burns_from_crowdsale_with_two_admins_multiple")

        def test05_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            set_dog_address_param = {
                "update_method": {"single": "set1"},
                "doga_address": BOB_PK,
            }

            multisig.removeAuthorizedContract(
                mint_controller.address).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                mint_controller.setDogaAddress(
                    set_dog_address_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMintController.print_success(
                "test05_it_fails_when_mint_controller_is_not_authorized_by_multisig")

        def test06_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            set_dog_address_param = {
                "update_method": {"single": "set1"},
                "doga_address": BOB_PK,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(mint_controller.address).setDogaAddress(
                    set_dog_address_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestMintController.print_success(
                "test06_it_fails_when_caller_is_not_an_admin")

        def test07_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            set_dog_address_param = {
                "update_method": {"single": "set1"},
                "doga_address": BOB_PK,
            }
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            mint_controller.setDogaAddress(
                set_dog_address_param).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                mint_controller.setDogaAddress(
                    set_dog_address_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestMintController.print_success(
                "test07_it_fails_when_called_twice_by_the_same_admin")

    class SetReserveAddress(unittest.TestCase):
        def test00_before_tests(self):
            TestMintController.print_title(self)

        def test01_it_sets_reserve_address_successfully_single(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            set_dog_address_param = {
                "update_method": {"single": "set1"},
                "reserve_address": BOB_PK,
            }
            self.assertNotEqual(minter.storage["reserve_address"](), BOB_PK)
            mint_controller.setReserveAddress(
                set_dog_address_param).send(**send_conf)
            self.assertEqual(minter.storage["reserve_address"](), BOB_PK)

            TestMintController.print_success(
                "test01_it_sets_reserve_address_successfully_single")

        def test02_it_sets_reserve_address_successfully_multiple(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            minter2_addr = factory.storage["contract_sets"]["set2"]["minter"]()
            minter2 = alice_pytezos.contract(minter2_addr)
            set_dog_address_param = {
                "update_method": {"multiple": ["set1", "set2"]},
                "reserve_address": BOB_PK,
            }
            self.assertNotEqual(minter.storage["reserve_address"](), BOB_PK)
            self.assertNotEqual(minter2.storage["reserve_address"](), BOB_PK)
            mint_controller.setReserveAddress(
                set_dog_address_param).send(**send_conf)
            self.assertEqual(minter.storage["reserve_address"](), BOB_PK)
            self.assertEqual(minter2.storage["reserve_address"](), BOB_PK)

            TestMintController.print_success(
                "test02_it_sets_reserve_address_successfully_multiple")

        def test03_it_sets_reserve_address_with_two_admins_single(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            set_dog_address_param = {
                "update_method": {"single": "set1"},
                "reserve_address": BOB_PK,
            }
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            self.assertNotEqual(minter.storage["reserve_address"](), BOB_PK)
            mint_controller.setReserveAddress(
                set_dog_address_param).send(**send_conf)
            self.assertNotEqual(minter.storage["reserve_address"](), BOB_PK)
            bob_pytezos.contract(mint_controller.address).setReserveAddress(
                set_dog_address_param).send(**send_conf)
            self.assertEqual(minter.storage["reserve_address"](), BOB_PK)

            TestMintController.print_success(
                "test03_it_sets_reserve_address_with_two_admins_single")

        def test04_it_sets_reserve_address_with_two_admins_multiple(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            minter2_addr = factory.storage["contract_sets"]["set2"]["minter"]()
            minter2 = alice_pytezos.contract(minter2_addr)
            set_dog_address_param = {
                "update_method": {"multiple": ["set1", "set2"]},
                "reserve_address": BOB_PK,
            }
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            self.assertNotEqual(minter.storage["reserve_address"](), BOB_PK)
            self.assertNotEqual(minter2.storage["reserve_address"](), BOB_PK)
            mint_controller.setReserveAddress(
                set_dog_address_param).send(**send_conf)
            self.assertNotEqual(minter.storage["reserve_address"](), BOB_PK)
            self.assertNotEqual(minter2.storage["reserve_address"](), BOB_PK)
            bob_pytezos.contract(mint_controller.address).setReserveAddress(
                set_dog_address_param).send(**send_conf)
            self.assertEqual(minter.storage["reserve_address"](), BOB_PK)
            self.assertEqual(minter2.storage["reserve_address"](), BOB_PK)

            TestMintController.print_success(
                "test04_it_sets_reserve_address_with_two_admins_multiple")

        def test05_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            set_dog_address_param = {
                "update_method": {"single": "set1"},
                "reserve_address": BOB_PK,
            }

            multisig.removeAuthorizedContract(
                mint_controller.address).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                mint_controller.setReserveAddress(
                    set_dog_address_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMintController.print_success(
                "test05_it_fails_when_mint_controller_is_not_authorized_by_multisig")

        def test06_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            set_dog_address_param = {
                "update_method": {"single": "set1"},
                "reserve_address": BOB_PK,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(mint_controller.address).setReserveAddress(
                    set_dog_address_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestMintController.print_success(
                "test06_it_fails_when_caller_is_not_an_admin")

        def test07_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            set_dog_address_param = {
                "update_method": {"single": "set1"},
                "reserve_address": BOB_PK,
            }
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            mint_controller.setReserveAddress(
                set_dog_address_param).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                mint_controller.setReserveAddress(
                    set_dog_address_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestMintController.print_success(
                "test07_it_fails_when_called_twice_by_the_same_admin")

    class UpdateFactory(unittest.TestCase):
        def test00_before_tests(self):
            TestMintController.print_title(self)

        def test01_it_updates_factory_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_factory = Env().deploy_factory(multisig=multisig.address)

            self.assertEqual(
                mint_controller.storage["factory"](), factory.address)
            mint_controller.updateFactory(
                new_factory.address).send(**send_conf)
            self.assertEqual(
                mint_controller.storage["factory"](), new_factory.address)

            TestMintController.print_success(
                "test01_it_updates_factory_successfully")

        def test02_it_updates_factory_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_factory = Env().deploy_factory(multisig=multisig.address)

            self.assertEqual(
                mint_controller.storage["factory"](), factory.address)

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            mint_controller.updateFactory(
                new_factory.address).send(**send_conf)
            self.assertEqual(
                mint_controller.storage["factory"](), factory.address)
            bob_pytezos.contract(mint_controller.address).updateFactory(
                new_factory.address).send(**send_conf)
            self.assertEqual(
                mint_controller.storage["factory"](), new_factory.address)
            TestMintController.print_success(
                "test02_it_updates_factory_with_two_admins")

        def test03_it_fails_when_nft_controller_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_factory = Env().deploy_factory(multisig=multisig.address)

            multisig.removeAuthorizedContract(
                mint_controller.address).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                mint_controller.updateFactory(
                    new_factory.address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMintController.print_success(
                "test03_it_fails_when_nft_controller_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_factory = Env().deploy_factory(multisig=multisig.address)

            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(mint_controller.address).updateFactory(
                    new_factory.address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestMintController.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_factory = Env().deploy_factory(multisig=multisig.address)
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            mint_controller.updateFactory(
                new_factory.address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                mint_controller.updateFactory(
                    new_factory.address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestMintController.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    def test_inner_test_class(self):
        test_classes_to_run = [
            self.SetMintParams,
            self.SetMintPerAddress,
            self.AddToWhitelist,
            self.RemoveFromWhitelist,
            self.BurnFromCrowdsale,
            self.SetNameList,
            self.SetDogaAddress,
            self.SetReserveAddress,
            self.UpdateFactory,
        ]
        suites_list = []
        for test_class in test_classes_to_run:
            suites_list.append(
                unittest.TestLoader().loadTestsFromTestCase(test_class))

        big_suite = unittest.TestSuite(suites_list)
        unittest.TextTestRunner().run(big_suite)

if __name__ == "__main__":
    unittest.main()
